#!/bin/bash

# Usage: ./ *.sh [--rule RULESTRING]


rulearg=`echo "$@" | grep -o "\\-\\-rule [^ ]*" | sed "s/\\-\\-rule\\ //"`

if ((${#rulearg} == 0)); then
rulearg="b3s23-ik"
echo "Rule unspecified; assuming b3s23-ik."
fi



vs=(1x256 2x128 4x64 8x32 C1 C2_4 C2_2 C2_1 C4_4 C4_1 D2_+2 D2_+1 D2_x D4_+4 D4_+2 D4_+1 D4_x4 D4_x1 D8_4 D8_1)
rule=$rulearg
for i in $(seq 0 ${#vs[*]})
do
i=${vs[i]}
echo $i
  ./recompile.sh --rule ${rule} --symmetry ${i}
 mv apgluxe ${rule}_${i}
done
