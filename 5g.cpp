#include <cstring>
#include <cstdio>
#include <random>
using namespace std;

bool aray[50][50];

const int glide[16] = {143, 346, 107, 286, 302, 115, 167, 370, 482, 181, 428, 241, 233, 412, 458, 157};

// 0: down right
// 1: down left
// 2: up left
// 3: up right

const int gc = 5;

inline bool inr(int x) { return 0 <= x && x <= 50; }

int main() {
    mt19937 rng; random_device rd;
    rng.seed(rd());
    int x = -1;
    while(x--) {
        memset(aray, 0, sizeof aray);
        for(int i=0; i<gc; i++) {
            int dx = rng()%20, dy = rng()%20, id=rng()%16;
            switch (id/4)
            {
                case 0: dx+=16; dy+=16; break;
                case 1: dx+=16; break;
                case 2: break;
                case 3: dy+=16; break;
                default: break;
            }
            bool f = false;
            for(int i=0;i<25;i++) if(inr(dx+i/5-1) && inr(dy+i%5-1) && aray[dx+i/5-1][dy+i%5-1]) { f = true; break; }
            if(f) continue;
            for(int i=0;i<9;i++) aray[dx+i/3][dy+i%3] |= (bool)(glide[id]&(1<<i));
        }
        string rle;
        for(int i=0; i<50; i++) {
            if(i) rle += '$';
            int cnt = 0, cv = 2;
            for(int j=0; j<50; j++) {
                if(aray[i][j] != cv) {
                    if(cv != 2) {
                        if(cnt != 1) rle += to_string(cnt);
                        rle += cv?'o':'b';
                    }
                    cnt = 1; cv = aray[i][j];
                } else cnt++;
            }
            if(cv) rle += (to_string(cnt)+(cv?'o':'b'));
        }
        while(rle.back() == '$') rle.pop_back();
        string srle;
        int cnt = 0;
        for(char c:rle) {
            if(c == '$') {
                if(srle.length()) cnt++;
            } else {
                if(cnt) srle += (to_string(cnt)+'$');
                cnt = 0;
                srle += c;
            }
        } 
        srle += '!';
        puts("x = 0, y = 0, rule = B3/S23");
        puts(srle.c_str());
    }
}
