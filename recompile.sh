#!/bin/bash

# Usage: ./recompile.sh [--rule RULESTRING]

rulearg=`echo "$@" | grep -o "\\-\\-rule [^ ]*" | sed "s/\\-\\-rule\\ //"`

if ((${#rulearg} == 0)); then
rulearg="b3s23"
echo "Rule unspecified; assuming b3s23."
fi

set -e

echo "Updating submodules..."
git submodule update --init --recursive




echo "Compiling 5g.cpp..."
g++ -o 5g 5g.cpp -O3 -Ofast -flto -march=native



echo "Configuring lifelib..."
cd apgmera
if command -v "python3" &>/dev/null; then
    echo "Using $(which python3) to configure lifelib..."
    python3 mkparams.py $rulearg "5G_stdin"
else
    echo "Using $(which python) to configure lifelib..."
    python mkparams.py $rulearg "5G_stdin"
fi
cd ..



rulearg='b3-ks23-i'
symmetry='5G_stdin'

echo "Compiling apgmera..."
apgmera/recompile.sh --$(rulearg)  --$(symmetry)

printf "\n\033[32;1m **** build process completed successfully **** \033[0m\n"


