# 5G_stdin

enumerating 5 gliders syntheses, and run it with apgsearch



compile with `g++ -o 5g 5g.cpp -O3 -Ofast -flto -march=native  -std=c++11`

complie apgmera with `./recompile.sh --rule b3-ks23-i --symmetry 5_glider_stdin_test`

running with `5g | ./apgluxe -n 10000000 -k rule110`


the 5g.cpp was written by testitemqlstudop 
https://conwaylife.com/forums/viewtopic.php?f=2&t=4257&sid=a5951d326684351e91e1e193663fcf2d


apgmera & lifelib is written by Adam P. Goucher
https://gitlab.com/apgoucher/

the recompile.sh is basically the same with Adam's ikpx2 
