# This Python file uses the following encoding: utf-8
# vote-tally-v0.7.py
# As of v0.7 there can be any number of spaces (including zero)
#   after the number and before the one, two, or three asterisks.

# import golly as g
votes = """

<!DOCTYPE html>
<html dir="ltr" lang="en-gb">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<meta name="LifeViewer" content="rle code 37 hide limit">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>PotY 2020 Voting Thread - ConwayLife.com</title>


	<link rel="canonical" href="https://conwaylife.com/forums/viewtopic.php?t=5066">

<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->

<style>
body{
	background-color:#FFF;
	background-image:url('https://www.conwaylife.com/images/back.png');
	background-position:center top;
}

#cl_navbar{font-size:150%;color:#222;position:relative;background-image:url('https://www.conwaylife.com/images/back_nav.png');background-position:center top;background-repeat:repeat-x;height:37px;margin-bottom:16px;margin-top:16px;line-height:37px;text-align:center;width:100%;border-right:1px solid #161646;border-left:1px solid #161646;}
#cl_navbar a{text-decoration:none;}
#cl_navbar a:hover{text-decoration:underline;}
</style>

<script type="text/javascript" src="./styles/prosilver/template/lv-plugin.js"></script>
<link href="./assets/css/font-awesome.min.css?assets_version=21" rel="stylesheet">
<link href="./styles/prosilver/theme/stylesheet.css?assets_version=21" rel="stylesheet">
<link href="./styles/prosilver/theme/en/stylesheet.css?assets_version=21" rel="stylesheet">




<!--[if lte IE 9]>
	<link href="./styles/prosilver/theme/tweaks.css?assets_version=21" rel="stylesheet">
<![endif]-->


<link href="./ext/phpbb/ads/styles/all/theme/phpbbads.css?assets_version=21" rel="stylesheet" media="screen" />


<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7884550756583428",
          enable_page_level_ads: true
     });
</script>

</head>
<body id="phpbb" class="nojs notouch section-viewtopic ltr ">


<div id="wrap" class="wrap">
	<a id="top" class="top-anchor" accesskey="t"></a>
	<div id="page-header">
		<div class="headerbar" role="banner">
					<div class="inner">

			<div id="site-description" class="site-description">
				<a id="logo" class="logo" href="./index.php" title="Board index"><img src="../images/forumlogo.png" width="45" height="45" alt="ConwayLife.com - A community for Conway's Game of Life and related cellular automata" /></a>
				<h1>ConwayLife.com</h1>
				<p>Forums for Conway's Game of Life</p>
				<p class="skiplink"><a href="#start_here">Skip to content</a></p>
			</div>

									<div id="search-box" class="search-box search-header" role="search">
				<form action="./search.php" method="get" id="search">
				<fieldset>
					<input name="keywords" id="keywords" type="search" maxlength="128" title="Search for keywords" class="inputbox search tiny" size="20" value="" placeholder="Search…" />
					<button class="button button-search" type="submit" title="Search">
						<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
					</button>
					<a href="./search.php" class="button button-search-end" title="Advanced search">
						<i class="icon fa-cog fa-fw" aria-hidden="true"></i><span class="sr-only">Advanced search</span>
					</a>
					
				</fieldset>
				</form>
			</div>
						
			</div>



					</div>
								<div id="cl_navbar">
									<a href="../">Home</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<a href="/wiki/">LifeWiki</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<b><a href="http://www.conwaylife.com/forums/">Forums</a></b>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<!-- <a href="/applet/">Web Applet</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp; --><a href="http://golly.sourceforge.net/">Download Golly</a>
				</div>
				<div class="navbar" role="navigation">
	<div class="inner">

	<ul id="nav-main" class="nav-main linklist" role="menubar">

		<li id="quick-links" class="quick-links dropdown-container responsive-menu" data-skip-responsive="true">
			<a href="#" class="dropdown-trigger">
				<i class="icon fa-bars fa-fw" aria-hidden="true"></i><span>Quick links</span>
			</a>
			<div class="dropdown">
				<div class="pointer"><div class="pointer-inner"></div></div>
				<ul class="dropdown-contents" role="menu">
					
											<li class="separator"></li>
																									<li>
								<a href="./search.php?search_id=unanswered" role="menuitem">
									<i class="icon fa-file-o fa-fw icon-gray" aria-hidden="true"></i><span>Unanswered topics</span>
								</a>
							</li>
							<li>
								<a href="./search.php?search_id=active_topics" role="menuitem">
									<i class="icon fa-file-o fa-fw icon-blue" aria-hidden="true"></i><span>Active topics</span>
								</a>
							</li>
							<li class="separator"></li>
							<li>
								<a href="./search.php" role="menuitem">
									<i class="icon fa-search fa-fw" aria-hidden="true"></i><span>Search</span>
								</a>
							</li>
					
										<li class="separator"></li>

									</ul>
			</div>
		</li>

				<li data-skip-responsive="true">
			<a href="/forums/app.php/help/faq" rel="help" title="Frequently Asked Questions" role="menuitem">
				<i class="icon fa-question-circle fa-fw" aria-hidden="true"></i><span>FAQ</span>
			</a>
		</li>
						
			<li class="rightside"  data-skip-responsive="true">
			<a href="./ucp.php?mode=login" title="Login" accesskey="x" role="menuitem">
				<i class="icon fa-power-off fa-fw" aria-hidden="true"></i><span>Login</span>
			</a>
		</li>
					<li class="rightside" data-skip-responsive="true">
				<a href="./ucp.php?mode=register" role="menuitem">
					<i class="icon fa-pencil-square-o  fa-fw" aria-hidden="true"></i><span>Register</span>
				</a>
			</li>
						</ul>

	<ul id="nav-breadcrumbs" class="nav-breadcrumbs linklist navlinks" role="menubar">
								<li class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
										<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope><a href="./index.php" itemtype="https://schema.org/Thing" itemprop="item" accesskey="h" data-navbar-reference="index"><i class="icon fa-home fa-fw"></i><span itemprop="name">Board index</span></a><meta itemprop="position" content="1" /></span>

											<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope data-forum-id="5"><a href="./viewforum.php?f=5" itemtype="https://schema.org/Thing" itemprop="item"><span itemprop="name">Game of Life</span></a><meta itemprop="position" content="2" /></span>
															<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope data-forum-id="2"><a href="./viewforum.php?f=2" itemtype="https://schema.org/Thing" itemprop="item"><span itemprop="name">Patterns</span></a><meta itemprop="position" content="3" /></span>
												</li>
		
					<li class="rightside responsive-search">
				<a href="./search.php" title="View the advanced search options" role="menuitem">
					<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
				</a>
			</li>
			</ul>

	</div>
</div>
	</div>



	
	<a id="start_here" class="anchor"></a>
	<div id="page-body" class="page-body" role="main">
		
		
<h2 class="topic-title"><a href="./viewtopic.php?f=2&amp;t=5066">PotY 2020 Voting Thread</a></h2>
<!-- NOTE: remove the style="display: none" when you want to have the forum description on the topic body -->
<div style="display: none !important;">For discussion of specific patterns or specific families of patterns, both newly-discovered and well-known.<br /></div>


<div class="action-bar bar-top">
	
			<a href="./posting.php?mode=reply&amp;f=2&amp;t=5066" class="button" title="Post a reply">
							<span>Post Reply</span> <i class="icon fa-reply fa-fw" aria-hidden="true"></i>
					</a>
	
			<div class="dropdown-container dropdown-button-control topic-tools">
		<span title="Topic tools" class="button button-secondary dropdown-trigger dropdown-select">
			<i class="icon fa-wrench fa-fw" aria-hidden="true"></i>
			<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
		</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																												<li>
					<a href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;view=print" title="Print view" accesskey="p">
						<i class="icon fa-print fa-fw" aria-hidden="true"></i><span>Print view</span>
					</a>
				</li>
											</ul>
		</div>
	</div>
	
			<div class="search-box" role="search">
			<form method="get" id="topic-search" action="./search.php">
			<fieldset>
				<input class="inputbox search tiny"  type="search" name="keywords" id="search_keywords" size="20" placeholder="Search this topic…" />
				<button class="button button-search" type="submit" title="Search">
					<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
				</button>
				<a href="./search.php" class="button button-search-end" title="Advanced search">
					<i class="icon fa-cog fa-fw" aria-hidden="true"></i><span class="sr-only">Advanced search</span>
				</a>
				<input type="hidden" name="t" value="5066" />
<input type="hidden" name="sf" value="msgonly" />

			</fieldset>
			</form>
		</div>
	
			<div class="pagination">
			58 posts
							<ul>
		<li class="active"><span>1</span></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" role="button">2</a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=50" role="button">3</a></li>
				<li class="arrow next"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" rel="next" role="button"><i class="icon fa-chevron-right fa-fw" aria-hidden="true"></i><span class="sr-only">Next</span></a></li>
	</ul>
					</div>
		</div>




			<div id="p123463" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123463">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts">4093</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123463">

						<h3 class="first"><a href="#p123463"><span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123463" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123463#p123463" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a></strong> &raquo; </span>February 22nd, 2021, 3:35 pm
			</p>
			
			
			
			<div class="content">Due to some of the entries having as many as twenty-one links, only the first link is provided for each entry. The full list of links can be seen at <a href="https://conwaylife.com/forums/viewtopic.php?f=2&amp;t=4968#p117558" class="postlink">the first post of the nominations thread</a>.
<ul>
<li><strong class="text-strong">#01 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;p=116491#p116491" class="postlink">Beehive push catalyst</a></strong>: a catalyst that allows the beehive push reaction, where a beehive affects an active region while being displaced by one cell orthogonally, to be used repeatedly in situations where there is not another synchronized mechanism to push it back, such as in conduits <em class="text-italics">(by Kazyan (Tanner Jacobi))</em></li>
<li><strong class="text-strong">#02 <a href="https://www.conwaylife.com/forums/viewtopic.php?t=&amp;p=93011#p93011" class="postlink">Natural loafers</a></strong>: Catagolue soups in the C1 and G1 symmetries that produce the twenty-cell loafer, making it the first spaceship not at the maximum speed for its direction to be found in a random asymmetric soup in B3S23 <em class="text-italics">(by John Goodman (John Goodman), rliston (Rob Liston), and calcyman (Adam P. Goucher))</em></li>
<li><strong class="text-strong">#03 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=87225#p87225" class="postlink">34-bit still life enumeration</a></strong>: an enumeration of all 34-bit still lifes and pseudo still lifes, which confirmed that the <a href="http://www.conwaylife.com/wiki/quad pseudo still life" class="postlink">quad pseudo still life</a> is the unique 34-bit pseudo still life requiring a decomposition into 4 or more pieces <em class="text-italics">(by Nathaniel (Nathaniel Johnston))</em></li>
<li><strong class="text-strong">#04 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=116260#p116260" class="postlink">c/61 near-spaceship and slide gun support</a></strong>: a reaction consisting of a pulsar interacting with two blocks that results in the same confirmation 61 generations later shifted by one cell orthogonally  if supplied with two dot sparks or certain fishhook-type interactions, which can be provided with gliders from slide guns <em class="text-italics">(by Kazyan (Tanner Jacobi) and Goldtiger997)</em></li>
<li><strong class="text-strong">#05 <a href="http://www.conwaylife.com/wiki/104P9" class="postlink">Period-9 heavyweight sparker and associated R shuttle</a></strong>: period-nine domino sparker that has two dot sparks displaced (2.5, 1) from the center of the domino spark, making it a heavyweight emulator, and has better clearance than the previous known period-nine domino sparker, allowing it and an eater 2 to form a periodic RFx9R conduit that can be attached to another copy of itself to form a shuttle <em class="text-italics">(wwei23, A for awesome (Aidan F. Pierce), and hotdogPi)</em></li>
<li><strong class="text-strong">#06 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;t=1437&amp;start=1575#p117606" class="postlink">p5 drifter-like catalyst/lightweight volcano/periodic catalyst</a></strong>: an oscillator that periodically creates then kills via overpopulation a "block face", allowing it to be used to perturb regions in the same manner as several catalysts without permanently harming the oscillator <em class="text-italics">(by wwei23 and Scorbie (Dongook Lee))</em></li>
<li><strong class="text-strong">#07 <a href="https://conwaylife.com/forums/viewtopic.php?f=2&amp;t=4968&amp;p=117609#p117609" class="postlink">New c/5 diagonal spaceships</a></strong>: several new c/5 diagonal spaceships of varying shapes <em class="text-italics">(by AforAmpere, Sarp, A for awesome (Aidan F. Pierce), winston (John Winston Garth), and Dylan Chen (Dylan Chen))</em></li>
<li><strong class="text-strong">#08 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;p=114020#p114020" class="postlink">New growing spaceships</a></strong>: patterns in which spaceships create a wick that is consumed at a slower speed than it is created at <em class="text-italics">(by Sokwe (Matthias Merzenich) and A for awesome (Aidan F. Pierce))</em></li>
<li><strong class="text-strong">#09 <a href="http://www.conwaylife.com/wiki/49768M" class="postlink">New longest-lasting 16x16 methuselah</a></strong>: an asymmetric 16x16 soup found with Catagolue that takes longer to stabilize than any previous assymetric 16x16 soup not forming Corderbackrakes and a related 16x16 pattern that takes one generation longer to form generation five of said random soup<em class="text-italics">(by rliston (Rob Liston) and Goldtiger997)</em></li>
<li><strong class="text-strong">#10 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;p=86970#p86970" class="postlink">New c/10 orthogonal technology</a></strong>: arrangements of copperheads and fireships that perform various tasks, such as firing a stream of loafers backwards <em class="text-italics">(by Mr. T (Christoph Reinthaler))</em></li>
<li><strong class="text-strong">#11 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=93912#p93912" class="postlink">Better universal regulator</a></strong>: a device that can align a set of input glider signals with no period constraints to any output period greater than 73 that is smaller and faster than any previous universal regulator <em class="text-italics">(by Jormungant (Louis-François Handfield))</em></li>
<li><strong class="text-strong">#12 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=89061#p89061" class="postlink">19-bit still life syntheses</a></strong>: glider syntheses for all 19-bit still lifes <em class="text-italics">(by Freywa (Jeremy Tan), dvgrn (Dave Greene), Hdjensofjfnen, Ian07, Kazyan (Tanner Jacobi), testitemqlstudop, chris_c (Chris Cain), BlinkerSpawn, Extrementhusiast (Martin Grant), Goldtiger997, wildmyron (Arie Paap), Macbi (Oscar Cunningham), Sokwe (Matthias Merzenich), GUYTU6J, and AGreason (Alex Greason))</em></li>
<li><strong class="text-strong">#13 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;t=1557&amp;p=88186#p88186" class="postlink">c/5 and c/6 guns</a></strong>: the first known guns for c/5 orthogonal, c/5 diagonal, and c/6 orthogonal spaceships <em class="text-italics">(by Entity Valkyrie, Goldtiger997, Extrementhusiast (Martin Grant), Kazyan (Tanner Jacobi), Freywa (Jeremy Tan), and wwei23)</em></li>
<li><strong class="text-strong">#14 <a href="https://www.conwaylife.com/forums/viewtopic.php?t=&amp;p=94346#p94346" class="postlink">Cheaper fixed-cost universal construction</a></strong>: reductions in the cost of reverse caber-tosser-based universal construction by figuring out how to split a target block with gliders instead of needing to use a block-laying switch engine, removing the need for a Sakapuffer, and reducing the number of glider-producing switch enginges required in the circuitry <em class="text-italics">(by chris_c (Chris Cain), calcyman (Adam P. Goucher), and MathAndCode)</em></li>
<li><strong class="text-strong">#15 <a href="http://www.conwaylife.com/wiki/409P6H1V0" class="postlink">Width-12 c/6 orthogonal spaceship</a></strong>: the first width-12 c/6 orthogonal spaceship, which has the minimum possible width for a period-6 spaceship of this speed <em class="text-italics">(by mscibing (Andrew J. Wade))</em></li>
<li><strong class="text-strong">#16 <a href="http://www.conwaylife.com/wiki/4-in-a-row%20Cordership" class="postlink">New small Corderships</a></strong>: several new three-engine and four-engine Corderships found using a custom apgsearch symmetry <em class="text-italics">(by wwei23 and Ian07)</em></li>
<li><strong class="text-strong">#17 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;p=110811#p110811" class="postlink">Period-10 glider shuttle</a></strong>: the lowest known period for a glider shuttle <em class="text-italics">(by Extrementhusiast (Martin Grant))</em></li>
<li><strong class="text-strong">#18 <a href="https://conwaylife.com/forums/viewtopic.php?p=104295#p104295" class="postlink">New agar crawler/agar bubble/skimmer/agar clearer technology</a></strong>: mechanisms to add, remove, travel along, or travel within agar <em class="text-italics">(by wildmyron (Arie Paap), BlinkerSpawn, dani, amling (Keith Amling), Saka, HartmutHolzwart (Hartmut Holzwart), and wwei23)</em></li>
<li><strong class="text-strong">#19 <a href="http://www.conwaylife.com/wiki/Speed%20Demonoid" class="postlink">Speed Demonoid</a></strong>: a self-constructing diagonal spaceship with speed limit c/4 due to using a crab wickstretcher <em class="text-italics">(by Pavgran (Pavel Grankovskiy), dvgrn (Dave Greene), Tanner Jacobi (Kazyan), and Goldtiger997)</em></li>
<li><strong class="text-strong">#20 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;p=117054#p117054" class="postlink">Extensible LoM hasslers</a></strong>: extensible oscillators based on lumps of muck <em class="text-italics">(by JP21, wwei23, and Sokwe (Matthias Merzenich))</em></li>
<li><strong class="text-strong">#21 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;p=95237#p95237" class="postlink">New c/6 diagonal pushalong</a></strong>: the first new piece of low-period c/6 diagonal technology in 9 years <em class="text-italics">(by A for awesome (Aidan F. Pierce))</em></li>
<li><strong class="text-strong">#22 <a href="http://www.conwaylife.com/wiki/Doo-dah" class="postlink">New 2c/7 technology</a></strong>: two new 2c/7 spaceships used to help make puffers and rakes <em class="text-italics">(by winston (John Winston Garth) and mscibing (Andrew J. Wade))</em></li>
<li><strong class="text-strong">#23 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=96210#p96210" class="postlink">4×N block agar construction</a></strong>: a way to extend the pseudo still life consisting of a 4×3 rectangle of blocks to a 4×N rectangle for arbitrarily high N using gliders <em class="text-italics">(by Goldtiger997)</em></li>
<li><strong class="text-strong">#24 <a href="http://www.conwaylife.com/wiki/Rob%27s%20p16" class="postlink">Rob's p16</a></strong>: a useful sparker found when it occurred in the ash of an assymetric Catagolue soup <em class="text-italics">(by rliston (Rob Liston))</em></li>
<li><strong class="text-strong">#25 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=108412#p108412" class="postlink">Loafer bitmap printer</a></strong>: a collection of loafer guns that can be toggled by a glider memory tape to produce a message in loafer-pixels <em class="text-italics">(by otismo)</em></li>
<li><strong class="text-strong">#26 <a href="https://conwaylife.com/forums/viewtopic.php?f=2&amp;t=4713&amp;start=50#p104435" class="postlink">Cordercolumn and extruder</a></strong>: two ways to stabilize a switch engine with another switch engine behind it and an extruder that emits a line of switch engines using one of the methods <em class="text-italics">(by wwei23, AbhpzTa (Luka Okanishi), and Ian07)</em></li>
<li><strong class="text-strong">#27 <a href="http://www.conwaylife.com/wiki/L122" class="postlink">Stable L122</a></strong>: a stable version of the L122 conduit, which previously only had periodic versions <em class="text-italics">(by A for awesome (Aidan F. Pierce))</em></li>
<li><strong class="text-strong">#28 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=112239#p112239" class="postlink">Stable barge-crosser</a></strong>: a way to transmit a glider signal across an infinitely long barge without permanently damaging it <em class="text-italics">(by Goldtiger997)</em></li>
<li><strong class="text-strong">#29 <a href="https://www.conwaylife.com/forums/viewtopic.php?t=&amp;p=92484#p92484" class="postlink">Small c/5 diagonal puffer</a></strong>: an elegant period-1540 c/5 diagonal puffer that uses only three copies of <a href="http://www.conwaylife.com/wiki/58P5H1V1" class="postlink">58P5H1V1</a> <em class="text-italics">(by Naszvadi (Peter Naszvadi))</em></li>
<li><strong class="text-strong">#30 <a href="https://conwaylife.com/forums/viewtopic.php?p=117591#p117591" class="postlink">New grayship technology</a></strong>: a large collection of new greyship components of varying speeds <em class="text-italics">(by amling (Keith Amling), HartmutHolzwart (Hartmut Holzwart), and Sokwe (Matthias Merzenich))</em></li>
<li><strong class="text-strong">#31 <a href="https://conwaylife.com/forums/viewtopic.php?f=2&amp;t=4257" class="postlink">Glider syntheses from Glider_stdin</a></strong>: glider synthesies based on a result from a custon Catagolue symmetry where gliders are fired at each other in a random configuration <em class="text-italics">(by testitemqlstudop, Kazyan (Tanner Jacobi), wildmyron (Arie Paap), GUYTU6J, and goldenratio)</em></li>
<li><strong class="text-strong">#32 <a href="https://www.conwaylife.com/forums/viewtopic.php?p=99834#p99834" class="postlink">New XWSS signal technology</a></strong>: improved mechanisms for detecting XWSS signals <em class="text-italics">(by dvgrn (Dave Greene), calcyman (Adam P. Goucher), and Goldtiger997)</em></li>
<li><strong class="text-strong">#33 <a href="http://www.conwaylife.com/wiki/Silverfish" class="postlink">Silverfish</a></strong>: a new smallest spaceship to use the 31c/240 Herschel climber <em class="text-italics">(by chris_c (Chris Cain), dvgrn (Dave Greene), calcyman (Adam P. Goucher), Nathaniel (Nathaniel Johnston))</em></li>
<li><strong class="text-strong">#34 <a href="http://www.conwaylife.com/wiki/Bandersnatch" class="postlink">"Bandersnatch" color-changing glider-to-glider conduit</a></strong>: a color-changing glider-to-glider conduit, which can be used to solve glider color mismatches <em class="text-italics">(by Extrementhusiast (Martin Grant) and Entity Valkyrie 2)</em></li>
<li><strong class="text-strong">#35 <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;t=315&amp;start=200#p100403" class="postlink">New exotic spaceship eaters and spaceship-to-G converters</a></strong>: new mechanisms to eat spaceships or turn them into glider signals <em class="text-italics">(by Freywa (Jeremy Tan), JP21, AbhpzTa (Luka Okanishi), dvgrn (Dave Greene), calcyman (Adam P. Goucher), Goldtiger997, Kazyan (Tanner Jacobi), EvinZL, bubblegum, A for awesome (Aidan F. Pierce), Jormungant (Louis-François Handfield), chris_c (Chris Cain), Layz Boi, Blinkerspawn, FWKnightship, Gustone, and gameoflifemaniac)</em></li>
<li><strong class="text-strong">#36 <a href="https://www.conwaylife.com/forums/viewtopic.php?t=&amp;p=90882#p90882" class="postlink">Record-setting diehards</a></strong>: patterns found from soup searches searches that take longer to settle without leaving any ash than any previously known patterns fitting in a bounding box of the same size <em class="text-italics">(by rliston (Rob Liston), goldenratio, and Moth Wingthane)</em></li>
<li><strong class="text-strong">#37 <a href="http://www.conwaylife.com/wiki/Anura" class="postlink">Smaller 3c/7 spaceships</a></strong>: the second and third known 3c/7 spaceships, both of which are smaller than the first known 3c/7 spaceship <em class="text-italics">(by Dylan Chen (Dylan Chen))</em></li>
<li><strong class="text-strong">#38 <a href="http://www.conwaylife.com/wiki/Period-28%20glider%20gun" class="postlink">Period-28 glider gun</a></strong>: the first true period-28 gun <em class="text-italics">(by Sokwe (Matthias Merzenich) and pcallahan (Paul Callahan))</em></li>
<li><strong class="text-strong">#39 <a href="https://www.conwaylife.com/forums/viewtopic.php?t=&amp;p=92323#p92323" class="postlink">Glider syntheses for 56P6H1V0, 58P5H1V1, turtle, 31P8H4V0, 44P5H2V0, 70P2H1V0.1, 55P9H3V0, and 44P5H2V0</a></strong>: ways to make various spaceships from gliders <em class="text-italics">(by Tanner Jacobi (Kazyan), Extrementhusiast (Martin Grant), Goldtiger997, AforAmpere, Blinkerspawn, calcyman (Adam P. Goucher), and mniemiec (Mark D. Niemiec))</em></li>
</ul>
<span style="color:red"><strong class="text-strong">To vote for a pattern</strong>, simply include a line where that pattern's number is followed by with a number of asterisks from one to three corresponding to how strongly you want to vote for that pattern. (Note that the patterns have been shuffled compared to the nominations thread, as is the standard.) If you do not want to vote for a pattern, simply do not include a line with its number.</span><br>
<br>
In case some of you are concerned about mixing up the numbers, here is a version optimized for voting. Simply replace the name of each pattern with the number of asterisks that you want to give it (and remove lines corresponding to patterns that you don't want to vote for).<br>
<br>
#01 Beehive push catalyst<br>
#02 Natural loafers<br>
#03 34-bit still life enumeration<br>
#04 c/61 near-spaceship<br>
#05 p9 heavyweight emulator<br>
#06 p5 lightweight emulator/catalyst<br>
#07 New c/5 diagonal spaceships<br>
#08 New growing spaceships<br>
#09 New longest 16x16 methuselah<br>
#10 New c/10 orthgonal technology<br>
#11 Better universal regulator<br>
#12 19-bit still life syntheses<br>
#13 c/5 and c/6 guns<br>
#14 Cheaper universal construction<br>
#15 Width-12 c/6 orthogonal spaceship<br>
#16 New small Corderships<br>
#17 p10 glider shuttle<br>
#18 New agar technology<br>
#19 Speed Demonoid<br>
#20 LoM hasslers<br>
#21 New c/6 diagonal pushalong<br>
#22 New 2c/7 technology<br>
#23 4×N block agar construction<br>
#24 Rob's p16<br>
#25 Loafer bitmap printer<br>
#26 Cordercolumn<br>
#27 Stable L122<br>
#28 Stable barge-crosser<br>
#29 Small c/5 diagonal puffer<br>
#30 New grayship technology<br>
#31 Glider syntheses from Glider_stdin<br>
#32 New XWSS signal technology<br>
#33 Silverfish<br>
#34 Bandersnatch<br>
#35 Spaceship eaters and spaceship-to-G converters<br>
#36 Record-setting diehards<br>
#37 Smaller 3c/7 spaceships<br>
#38 p28 gun<br>
#39 Spaceship glider syntheses</div>

			
			
													<div class="notice">
					Last edited by <a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a> on February 22nd, 2021, 6:29 pm, edited 2 times in total.
									</div>
			
									<div id="sig123463" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
				
			<div id="p123466" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123466">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2570" class="avatar"><img class="avatar" src="./download/file.php?avatar=2570_1609346478.gif" width="98" height="98" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2570" class="username">C28</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2570&amp;sr=posts">56</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> December 8th, 2020, 12:23 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> 1/0</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123466">

						<h3 ><a href="#p123466">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123466" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123466#p123466" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2570" class="username">C28</a></strong> &raquo; </span>February 22nd, 2021, 3:50 pm
			</p>
			
			
			
			<div class="content">#02 ***<br>
#07 **<br>
#09 ***<br>
#20 *<br>
#29 **<br>
#36 ***<br>
#37 *<br>
#38 **</div>

			
			
									
									<div id="sig123466" class="signature">working on making a <a href="https://www.conwaylife.com/forums/viewtopic.php?f=2&amp;t=1539" class="postlink">(13,4)c/41</a> spaceship</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123468" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123468">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2450" class="avatar"><img class="avatar" src="./download/file.php?avatar=2450_1597605545.gif" width="22" height="23" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2450" class="username">goldenratio</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2450&amp;sr=posts">277</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> July 26th, 2020, 10:39 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123468">

						<h3 ><a href="#p123468">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123468" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123468#p123468" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2450" class="username">goldenratio</a></strong> &raquo; </span>February 22nd, 2021, 3:59 pm
			</p>
			
			
			
			<div class="content">#01 *<br>
#02 **<br>
#04 *<br>
#05 **<br>
#07 *<br>
#08 *<br>
#09 **<br>
#10 *<br>
#12 **<br>
#13 **<br>
#14 **<br>
#15 *<br>
#16 **<br>
#18 *<br>
#19 ***<br>
#21 *<br>
#22 ***<br>
#23 *<br>
#24 ***<br>
#26 *<br>
#27 *<br>
#28 **<br>
#29 *<br>
#30 *<br>
#31 **<br>
#33 ***<br>
#34 ***<br>
#35 **<br>
#36 **<br>
#37 ***<br>
#38 ***<br>
#39 ***</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123469" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123469">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1665" class="avatar"><img class="avatar" src="./download/file.php?avatar=1665_1501780000.gif" width="58" height="62" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1665" class="username">toroidalet</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1665&amp;sr=posts">1236</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 7th, 2016, 1:48 pm</dd>		
		
																<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> My computer</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact toroidalet">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact toroidalet</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="https://www.com" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123469">

						<h3 ><a href="#p123469">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123469" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123469#p123469" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1665" class="username">toroidalet</a></strong> &raquo; </span>February 22nd, 2021, 4:14 pm
			</p>
			
			
			
			<div class="content">#01 *<br>
#02 **<br>
#04 *<br>
#09 **<br>
#14 ***<br>
#19 ***<br>
#24 **<br>
#33 **<br>
#34 **<br>
#38 **<br>
#39 ***</div>

			
			
									
									<div id="sig123469" class="signature">"But if you close your eyes—does it almost feel like nothing's changed at all?<br>
And if you close your eyes—does it almost feel like you've been here before..."</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123470" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123470">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2095" class="avatar"><img class="avatar" src="./download/file.php?avatar=2095_1548629855.jpg" width="128" height="115" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2095" class="username">Moosey</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2095&amp;sr=posts">4133</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> January 27th, 2019, 5:54 pm</dd>		
		
																<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> A house, or perhaps the OCA board. Or [click to not expand]</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact Moosey">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact Moosey</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://www.conwaylife.com/wiki/User:Moosey" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123470">

						<h3 ><a href="#p123470">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123470" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123470#p123470" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2095" class="username">Moosey</a></strong> &raquo; </span>February 22nd, 2021, 4:18 pm
			</p>
			
			
			
			<div class="content">#1 **<br>
#2 *<br>
#3 *<br>
#4 **<br>
#5 *<br>
#6 *<br>
#7 ***<br>
#8 **<br>
#9 **<br>
#10 **<br>
#11 **<br>
#12 *<br>
#13 **<br>
#14 ***<br>
#15 **<br>
#16 ***<br>
#17 *<br>
#18 *<br>
#19 ***<br>
#20 *<br>
#21 **<br>
#22 ***<br>
#23 *<br>
#24 ***<br>
#25 *<br>
#26 **<br>
#27 **<br>
#28 **<br>
#29 ***<br>
#30 **<br>
#31 ***<br>
#32 **<br>
#33 ***<br>
#34 ***<br>
#35 **<br>
#36 *<br>
#37 ***<br>
#38 **<br>
#39 ***</div>

			
			
									
									<div id="sig123470" class="signature">My CA rules can be found <a href="http://www.conwaylife.com/wiki/User:Moosey/Moosey%27s_Rules" class="postlink">here</a><br>
<br>
Bill Watterson once wrote: "How do soldiers killing each other solve the world's problems?"<br>
Nanho walåt derwo esaato?<br>
<br>
leaplife advertising bad!</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123472" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123472">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2015" class="avatar"><img class="avatar" src="./download/file.php?avatar=2015_1599664861.gif" width="97" height="57" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2015" class="username">Ian07</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2015&amp;sr=posts">679</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> September 22nd, 2018, 8:48 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123472">

						<h3 ><a href="#p123472">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123472" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123472#p123472" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2015" class="username">Ian07</a></strong> &raquo; </span>February 22nd, 2021, 4:32 pm
			</p>
			
			
			
			<div class="content">#01 *<br>
#02 **<br>
#03 *<br>
#04 *<br>
#05 **<br>
#06 *<br>
#07 **<br>
#08 **<br>
#09 **<br>
#10 *<br>
#11 *<br>
#12 ***<br>
#13 **<br>
#14 ***<br>
#15 **<br>
#16 **<br>
#17 *<br>
#18 *<br>
#19 **<br>
#20 **<br>
#21 *<br>
#22 ***<br>
#23 **<br>
#24 ***<br>
#25 *<br>
#26 **<br>
#27 **<br>
#28 **<br>
#29 **<br>
#30 *<br>
#31 ***<br>
#32 *<br>
#33 **<br>
#34 **<br>
#35 **<br>
#36 **<br>
#37 ***<br>
#38 **<br>
#39 ***</div>

			
			
									
									<div id="sig123472" class="signature">Wiki: <a href="http://www.conwaylife.com/wiki/User:Ian07" class="postlink">http://www.conwaylife.com/wiki/User:Ian07</a><br>
Discord: Ian07#6028<br>
<a href="https://catagolue.appspot.com/object/xs24_69bobjob96/b3s23" class="postlink">xs24_69bobjob96</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123474" class="post has-profile bg2 online">
		<div class="inner">

		<dl class="postprofile" id="profile123474">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2041" class="avatar"><img class="avatar" src="./download/file.php?avatar=2041_1587392934.gif" width="91" height="91" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2041" class="username">EvinZL</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2041&amp;sr=posts">407</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> November 8th, 2018, 4:15 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> What is &quot;location&quot;?</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123474">

						<h3 ><a href="#p123474">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123474" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123474#p123474" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2041" class="username">EvinZL</a></strong> &raquo; </span>February 22nd, 2021, 5:03 pm
			</p>
			
			
			
			<div class="content">Votes:<br>
#1 ***<br>
#2 *<br>
#5 ***<br>
#6 **<br>
#10 *<br>
#11 **<br>
#14 **<br>
#16 *<br>
#17 ***<br>
#19 ***<br>
#22 **<br>
#24 **<br>
#26 *<br>
#27 ***<br>
#28 *<br>
#29 *<br>
#32 *<br>
#33 ***<br>
#34 ***<br>
#35 **<br>
#37 **<br>
#38 ***<br>
<br>
EDIT: Current tabulations, as of sokwe's vote
<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>{
  "Beehive push catalyst": 34,
  "Natural loafers": 45,
  "34-bit still life enumeration": 24,
  "c/61 near-spaceship": 24,
  "p9 heavyweight emulator": 28,
  "p5 lightweight emulator/catalyst": 21,
  "New c/5 diagonal spaceships": 36,
  "New growing spaceships": 18,
  "New longest 16x16 methuselah": 32,
  "New c/10 orthgonal technology": 29,
  "Better universal regulator": 28,
  "19-bit still life syntheses": 45,
  "c/5 and c/6 guns": 33,
  "Cheaper universal construction": 50,
  "Width-12 c/6 orthogonal spaceship": 24,
  "New small Corderships": 31,
  "p10 glider shuttle": 18,
  "New agar technology": 31,
  "Speed Demonoid": 59,
  "LoM hasslers": 18,
  "New c/6 diagonal pushalong": 23,
  "New 2c/7 technology": 45,
  "4×N block agar construction": 23,
  "Rob's p16": 53,
  "Loafer bitmap printer": 25,
  "Cordercolumn": 27,
  "Stable L122": 28,
  "Stable barge-crosser": 41,
  "Small c/5 diagonal puffer": 32,
  "New grayship technology": 37,
  "Glider syntheses from Glider_stdin": 30,
  "New XWSS signal technology": 24,
  "Silverfish": 43,
  "Bandersnatch": 56,
  "Spaceship eaters and spaceship-to-G converters": 29,
  "Record-setting diehards": 26,
  "Smaller 3c/7 spaceships": 54,
  "p28 gun": 51,
  "Spaceship glider syntheses": 52
}
</code></pre></div></div>

			
			
													<div class="notice">
					Last edited by <a href="./memberlist.php?mode=viewprofile&amp;u=2041" class="username">EvinZL</a> on March 7th, 2021, 9:43 pm, edited 11 times in total.
									</div>
			
									<div id="sig123474" class="signature"><div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 59, y = 12, rule = B2i3-kq4j/S2-i3
.2A7.2A35.2A7.2A$3A7.3A15.3A15.3A7.3A$.2A7.2A15.A3.A15.2A7.2A$26.A5.A
$26.A5.A$26.3A.3A2$26.3A.3A$26.A5.A$26.A5.A$27.A3.A$28.3A!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123476" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123476">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="avatar"><img class="avatar" src="./download/file.php?avatar=2184_1614217273.png" width="100" height="100" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="username">bubblegum</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2184&amp;sr=posts">825</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 25th, 2019, 11:59 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> click here to do nothing</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123476">

						<h3 ><a href="#p123476">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123476" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123476#p123476" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="username">bubblegum</a></strong> &raquo; </span>February 22nd, 2021, 5:13 pm
			</p>
			
			
			
			<div class="content">#01 **<br>
#05 **<br>
#06 **<br>
#07 **<br>
#09 *<br>
#10 **<br>
#12 ***<br>
#13 **<br>
#15 *<br>
#16 **<br>
#17 *<br>
#18 **<br>
#19 *<br>
#20 *<br>
#21 **<br>
#22 ***<br>
#24 ***<br>
#26 **<br>
#27 *<br>
#28 ***<br>
#29 ***<br>
#30 *<br>
#31 **<br>
#34 **<br>
#35 *<br>
#36 *<br>
#37 ***<br>
#38 **<br>
#39 ***</div>

			
			
									
									<div id="sig123476" class="signature">Each day is a hidden opportunity, a frozen waterfall that's waiting to be realised, and one that I'll probably be ignoring
<blockquote><div><cite>sonata wrote:<div class="responsive-hide">July 2nd, 2020, 8:33 pm</div></cite>conwaylife signatures are amazing[<a href="https://xkcd.com/285" class="postlink">citation needed</a>]</div></blockquote><a href="https://conwaylife.com/wiki/User:Bubblegum" class="postlink">anything</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123477" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123477">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2565" class="avatar"><img class="avatar" src="./download/file.php?avatar=2565_1607023222.jpg" width="128" height="77" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2565" class="username">winston</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2565&amp;sr=posts">2</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> December 1st, 2020, 1:26 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123477">

						<h3 ><a href="#p123477">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123477" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123477#p123477" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2565" class="username">winston</a></strong> &raquo; </span>February 22nd, 2021, 5:15 pm
			</p>
			
			
			
			<div class="content">#2 ***<br>
#12 ***<br>
#18 *<br>
#22 ***<br>
#24 **<br>
#30 *<br>
#37 **<br>
#38 *<br>
#39 **</div>

			
			
									
									<div id="sig123477" class="signature">John Winston Garth</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123478" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123478">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2482" class="avatar"><img class="avatar" src="./download/file.php?avatar=2482_1598728310.jpg" width="128" height="127" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2482" class="username">iNoMed</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2482&amp;sr=posts">109</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 29th, 2020, 3:05 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Scotland</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123478">

						<h3 ><a href="#p123478">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123478" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123478#p123478" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2482" class="username">iNoMed</a></strong> &raquo; </span>February 22nd, 2021, 5:20 pm
			</p>
			
			
			
			<div class="content">#1 **<br>
#2 ***<br>
#4 *<br>
#5 *<br>
#7 *<br>
#10 *<br>
#12 **<br>
#14 ***<br>
#16 *<br>
#19 ***<br>
#21 *<br>
#22 **<br>
#24 ***<br>
#27 **<br>
#29 *<br>
#31 *<br>
#33 **<br>
#34 ***<br>
#37 **<br>
#38 **<br>
#39 ***</div>

			
			
									
									<div id="sig123478" class="signature"><div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 35, y = 5, rule = B3/S23
4b2o3b2o3bo3b2ob2o3b2ob2o$2o2bobo3bo2bobobobobobobobobobo$obobo2bo2bob
o2bobo2bo2bobo3bobo$2bobo3bobobobo2bo5bobo3bobob2o$b2ob2o3b2ob2o3b2o3b
2ob2o3b2ob2o!</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123480" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123480">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="avatar"><img class="avatar" src="./download/file.php?avatar=2614_1614092063.gif" width="100" height="125" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2614&amp;sr=posts">238</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 18th, 2021, 11:18 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123480">

						<h3 ><a href="#p123480">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123480" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123480#p123480" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a></strong> &raquo; </span>February 22nd, 2021, 5:33 pm
			</p>
			
			
			
			<div class="content">I'll give each pattern three stars.</div>

			
			
									
									<div id="sig123480" class="signature"><div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 19, y = 17, rule = Symbiosis
11.A$9.2A.A$3.A$2.3A3.A6.A$.A2.A3.2A5.A$A7.A.3A2.2A$A.3A4.A.4A.3A$.2A
2.B3.A4.A$B.B8.2A$13.A$12.2A$7.B3.2A4.2B$8.2A.A5.A$10.A3.A$9.2AB.B$8.
A2.2B4.B$8.B9.A!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123482" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123482">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="avatar"><img class="avatar" src="./download/file.php?avatar=2184_1614217273.png" width="100" height="100" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="username">bubblegum</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2184&amp;sr=posts">825</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 25th, 2019, 11:59 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> click here to do nothing</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123482">

						<h3 ><a href="#p123482">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123482" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123482#p123482" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="username">bubblegum</a></strong> &raquo; </span>February 22nd, 2021, 5:49 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614">wwei47</a> wrote: <a href="./viewtopic.php?p=123480#p123480" data-post-id="123480" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 5:33 pm</div></cite>
I'll give each pattern three stars.
</div></blockquote>

Note for future voters: <span style="color:#FF0000">do <em class="text-italics">not</em> vote like this:</span><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=1549">muzik</a> wrote: <a href="./viewtopic.php?p=67770#p67770" data-post-id="67770" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">January 12th, 2019, 6:53 pm</div></cite>Three stars for 9 (obviously), 1, 13, 16, 22 and 34, two for 8, 11, 17, 19, 23, 27, 31, and 1 for the rest.</div></blockquote>

A <span class="posthilit">script</span>'s doing the counting here.</div>

			
			
									
									<div id="sig123482" class="signature">Each day is a hidden opportunity, a frozen waterfall that's waiting to be realised, and one that I'll probably be ignoring
<blockquote><div><cite>sonata wrote:<div class="responsive-hide">July 2nd, 2020, 8:33 pm</div></cite>conwaylife signatures are amazing[<a href="https://xkcd.com/285" class="postlink">citation needed</a>]</div></blockquote><a href="https://conwaylife.com/wiki/User:Bubblegum" class="postlink">anything</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123484" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123484">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=141" class="avatar"><img class="avatar" src="./download/file.php?avatar=141_1504612401.jpg" width="64" height="64" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=141" class="username">Macbi</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=141&amp;sr=posts">792</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> March 29th, 2009, 4:58 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123484">

						<h3 ><a href="#p123484">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123484" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123484#p123484" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=141" class="username">Macbi</a></strong> &raquo; </span>February 22nd, 2021, 5:56 pm
			</p>
			
			
			
			<div class="content">#01 *<br>
#02 **<br>
#03 **<br>
#07 *<br>
#09 **<br>
#11 *<br>
#12 **<br>
#14 ***<br>
#18 **<br>
#19 ***<br>
#23 **<br>
#24 **<br>
#26 *<br>
#28 *<br>
#30 *<br>
#31 *<br>
#32 *<br>
#33 *<br>
#34 **<br>
#35 *<br>
#36 *<br>
#37 *<br>
#38 **<br>
#39 **</div>

			
			
													<div class="notice">
					Last edited by <a href="./memberlist.php?mode=viewprofile&amp;u=141" class="username">Macbi</a> on February 22nd, 2021, 6:09 pm, edited 1 time in total.
									</div>
			
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123486" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123486">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2056" class="avatar"><img class="avatar" src="./download/file.php?avatar=2056_1609515779.gif" width="105" height="56" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2056" class="username">creeperman7002</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2056&amp;sr=posts">242</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> December 4th, 2018, 11:52 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123486">

						<h3 ><a href="#p123486">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123486" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123486#p123486" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2056" class="username">creeperman7002</a></strong> &raquo; </span>February 22nd, 2021, 6:01 pm
			</p>
			
			
			
			<div class="content">#02 ***<br>
#03 *<br>
#09 ***<br>
#12 **<br>
#13 **<br>
#14 ***<br>
#19 **<br>
#21 *<br>
#24 ***<br>
#29 **<br>
#33 *<br>
#34 **<br>
#36 ***<br>
#37 ***<br>
#38 **<br>
#39 **</div>

			
			
									
									<div id="sig123486" class="signature">B2n3-jn/S1c23-y is an interesting rule. It has a replicator, a fake glider, an OMOS and SMOS, a wide variety of oscillators, and some signals. Also this rule is omniperiodic.<br>
<a href="https://www.conwaylife.com/forums/viewtopic.php?f=11&amp;t=4856" class="postlink">viewtopic.php?f=11&amp;t=4856</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123489" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123489">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts">4093</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123489">

						<h3 ><a href="#p123489">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123489" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123489#p123489" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a></strong> &raquo; </span>February 22nd, 2021, 6:08 pm
			</p>
			
			
			
			<div class="content">#01 ***<br>
#04 *<br>
#05 **<br>
#06 ***<br>
#07 *<br>
#10 *<br>
#11 **<br>
#12 *<br>
#14 ***<br>
#16 **<br>
#19 **<br>
#20 *<br>
#22 **<br>
#24 **<br>
#26 **<br>
#27 ***<br>
#32 *<br>
#34 ***<br>
#35 *<br>
#37 ***<br>
#38 ***<br>
#39 *</div>

			
			
									
									<div id="sig123489" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123494" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123494">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="avatar"><img class="avatar" src="./download/file.php?avatar=2614_1614092063.gif" width="100" height="125" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2614&amp;sr=posts">238</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 18th, 2021, 11:18 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123494">

						<h3 ><a href="#p123494">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123494" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123494#p123494" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a></strong> &raquo; </span>February 22nd, 2021, 6:59 pm
			</p>
			
			
			
			<div class="content">#01 ***<br>
#02 ***<br>
#03 ***<br>
#04 ***<br>
#05 ***<br>
#06 ***<br>
#07 ***<br>
#08 ***<br>
#09 ***<br>
#10 ***<br>
#11 ***<br>
#12 ***<br>
#13 ***<br>
#14 ***<br>
#15 ***<br>
#16 ***<br>
#17 ***<br>
#18 ***<br>
#19 ***<br>
#20 ***<br>
#21 ***<br>
#22 ***<br>
#23 ***<br>
#24 ***<br>
#25 ***<br>
#26 ***<br>
#27 ***<br>
#28 ***<br>
#29 ***<br>
#30 ***<br>
#31 ***<br>
#32 ***<br>
#33 ***<br>
#34 ***<br>
#35 ***<br>
#36 ***<br>
#37 ***<br>
#38 ***<br>
#39 ***</div>

			
			
									
									<div id="sig123494" class="signature"><div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 19, y = 17, rule = Symbiosis
11.A$9.2A.A$3.A$2.3A3.A6.A$.A2.A3.2A5.A$A7.A.3A2.2A$A.3A4.A.4A.3A$.2A
2.B3.A4.A$B.B8.2A$13.A$12.2A$7.B3.2A4.2B$8.2A.A5.A$10.A3.A$9.2AB.B$8.
A2.2B4.B$8.B9.A!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123495" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123495">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=762" class="avatar"><img class="avatar" src="./download/file.php?avatar=762_1575276963.gif" width="41" height="41" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=762" class="username">otismo</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=762&amp;sr=posts">640</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 18th, 2010, 1:41 pm</dd>		
		
																					<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Florida</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact otismo">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact otismo</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://foxeo.com" title="Website">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																																																<a href="ymsgr:sendim?foxeo@ymail.com" title="Yahoo Messenger" class="last-cell">
										<span class="contact-icon phpbb_yahoo-icon">Yahoo Messenger</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123495">

						<h3 ><a href="#p123495">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123495" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123495#p123495" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=762" class="username">otismo</a></strong> &raquo; </span>February 22nd, 2021, 7:01 pm
			</p>
			
			
			
			<div class="content">#10 ***<br>
#25 ***<br>
#28 ***<br>
#34 ***</div>

			
			
													<div class="notice">
					Last edited by <a href="./memberlist.php?mode=viewprofile&amp;u=762" class="username">otismo</a> on February 23rd, 2021, 8:42 am, edited 1 time in total.
									</div>
			
									<div id="sig123495" class="signature">In the Game of Life there are no winners there are no losers there are only SURVIVORS.<br>
.....**<br>
...****<br>
.******<br>
*( ͡° ͜ʖ ͡°)*
<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>#CXRLE Pos=0,0
x = 26, y = 34, rule = B3/S23
23bo$23bobo$23b2o9$2b3o2$o5bo$o5bo$o5bo2$2b3o5b2o$10b2o3$2b2o$2b2o10$
15b2o$15b2o!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123497" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123497">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts">4093</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123497">

						<h3 ><a href="#p123497">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123497" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123497#p123497" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a></strong> &raquo; </span>February 22nd, 2021, 7:34 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614">wwei47</a> wrote: <a href="./viewtopic.php?p=123480#p123480" data-post-id="123480" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 5:33 pm</div></cite>
I'll give each pattern three stars.
</div></blockquote>
Doesn't that remove the point of voting?</div>

			
			
									
									<div id="sig123497" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123498" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123498">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1306" class="avatar"><img class="avatar" src="./download/file.php?avatar=1306_1589928695.gif" width="97" height="97" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1306" class="username">A for awesome</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1306&amp;sr=posts">2333</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> September 13th, 2014, 5:36 pm</dd>		
		
																<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Pembina University, Home of the Gliders</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact A for awesome">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact A for awesome</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://www.example.com/" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123498">

						<h3 ><a href="#p123498">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123498" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123498#p123498" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1306" class="username">A for awesome</a></strong> &raquo; </span>February 22nd, 2021, 7:52 pm
			</p>
			
			
			
			<div class="content">#01 **<br>
#02 **<br>
#03 *<br>
#05 *<br>
#09 *<br>
#10 *<br>
#12 **<br>
#13 *<br>
#14 ***<br>
#16 *<br>
#18 **<br>
#19 ***<br>
#21 *<br>
#22 **<br>
#23 *<br>
#24 ***<br>
#25 *<br>
#26 *<br>
#28 *<br>
#30 *<br>
#33 *<br>
#34 ***<br>
#35 *<br>
#36 *<br>
#37 **<br>
#38 **<br>
#39 **</div>

			
			
									
									<div id="sig123498" class="signature">praosylen#5847 (Discord)<br>
<br>
The only decision I made was made<br>
of flowers, to jump universes to one of springtime in<br>
a land of former winter, where no invisible walls stood,<br>
or could stand for more than a few hours at most...</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123499" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123499">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="avatar"><img class="avatar" src="./download/file.php?avatar=2614_1614092063.gif" width="100" height="125" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2614&amp;sr=posts">238</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 18th, 2021, 11:18 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123499">

						<h3 ><a href="#p123499">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123499" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123499#p123499" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a></strong> &raquo; </span>February 22nd, 2021, 7:53 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486">MathAndCode</a> wrote: <a href="./viewtopic.php?p=123497#p123497" data-post-id="123497" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 7:34 pm</div></cite>
<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614">wwei47</a> wrote: <a href="./viewtopic.php?p=123480#p123480" data-post-id="123480" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 5:33 pm</div></cite>
I'll give each pattern three stars.
</div></blockquote>
Doesn't that remove the point of voting?
</div></blockquote>
I guess so, but it takes a lot of effort to produce those patterns, and for that, I give them 3 stars.</div>

			
			
									
									<div id="sig123499" class="signature"><div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 19, y = 17, rule = Symbiosis
11.A$9.2A.A$3.A$2.3A3.A6.A$.A2.A3.2A5.A$A7.A.3A2.2A$A.3A4.A.4A.3A$.2A
2.B3.A4.A$B.B8.2A$13.A$12.2A$7.B3.2A4.2B$8.2A.A5.A$10.A3.A$9.2AB.B$8.
A2.2B4.B$8.B9.A!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123500" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123500">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="avatar"><img class="avatar" src="./download/file.php?avatar=2184_1614217273.png" width="100" height="100" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="username">bubblegum</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2184&amp;sr=posts">825</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 25th, 2019, 11:59 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> click here to do nothing</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123500">

						<h3 ><a href="#p123500">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123500" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123500#p123500" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2184" class="username">bubblegum</a></strong> &raquo; </span>February 22nd, 2021, 8:06 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614">wwei47</a> wrote: <a href="./viewtopic.php?p=123499#p123499" data-post-id="123499" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 7:53 pm</div></cite>
I guess so, but it takes a lot of effort to produce those patterns, and for that, I give them 3 stars.
</div></blockquote>

It also took a lot of effort to create the pair of headphones that is currently producing a large amount of tritonic white noise because the designers screwed up and forgot to include a turn-off button.<br>
<br>
Silly analogy, granted, but that isn't really a reason to vote at all. It's more about what your opinion on the finished product is, never mind how hard it was to come to.</div>

			
			
									
									<div id="sig123500" class="signature">Each day is a hidden opportunity, a frozen waterfall that's waiting to be realised, and one that I'll probably be ignoring
<blockquote><div><cite>sonata wrote:<div class="responsive-hide">July 2nd, 2020, 8:33 pm</div></cite>conwaylife signatures are amazing[<a href="https://xkcd.com/285" class="postlink">citation needed</a>]</div></blockquote><a href="https://conwaylife.com/wiki/User:Bubblegum" class="postlink">anything</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123501" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123501">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts">4093</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123501">

						<h3 ><a href="#p123501">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123501" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123501#p123501" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a></strong> &raquo; </span>February 22nd, 2021, 8:21 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614">wwei47</a> wrote: <a href="./viewtopic.php?p=123499#p123499" data-post-id="123499" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 7:53 pm</div></cite>I guess so, but it takes a lot of effort to produce those patterns, and for that, I give them 3 stars.
</div></blockquote>
Yes, but based on the fact that people bother having this vote, I doubt that everyone is supposed to vote like that.</div>

			
			
									
									<div id="sig123501" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123505" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123505">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1453" class="avatar"><img class="avatar" src="./download/file.php?avatar=1453_1584446395.gif" width="73" height="97" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1453" class="username">Saka</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1453&amp;sr=posts">3619</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> June 19th, 2015, 8:50 pm</dd>		
		
																<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Indonesia</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact Saka">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact Saka</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="https://www.conwaylife.com/forums/memberlist.php?mode=viewprofile&amp;u=1453" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123505">

						<h3 ><a href="#p123505">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123505" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123505#p123505" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1453" class="username">Saka</a></strong> &raquo; </span>February 22nd, 2021, 9:59 pm
			</p>
			
			
			
			<div class="content">#02 ***<br>
#03 **<br>
#04 **<br>
#07 **<br>
#08 **<br>
#09 **<br>
#10 *<br>
#12 *<br>
#13 *<br>
#14 *<br>
#15 **<br>
#16 **<br>
#18 ***<br>
#19 **<br>
#21 *<br>
#22 ***<br>
#23 **<br>
#24 **<br>
#25 **<br>
#28 ***<br>
#29 *<br>
#30 ***<br>
#31 **<br>
#33 **<br>
#36 **<br>
#37 ***<br>
#39 **</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123512" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123512">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2142" class="avatar"><img class="avatar" src="./download/file.php?avatar=2142_1566627510.gif" width="114" height="114" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2142" class="username">PC101</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2142&amp;sr=posts">124</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> May 9th, 2019, 11:32 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> :noiɟɒɔo⅃</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123512">

						<h3 ><a href="#p123512">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123512" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123512#p123512" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2142" class="username">PC101</a></strong> &raquo; </span>February 22nd, 2021, 10:51 pm
			</p>
			
			
			
			<div class="content">#04 **<br>
#07 **<br>
#08 *<br>
#10 **<br>
#15 *<br>
#16 **<br>
#18 **<br>
#19 ***<br>
#22 **<br>
#25 **<br>
#26 **<br>
#28 **<br>
#29 *<br>
#30 **<br>
#33 ***<br>
#35 *<br>
#37 **<br>
#38 **</div>

			
			
									
									<div id="sig123512" class="signature">Puffer Suppressor<br>
Would we be able to know when we know everything there is to know?<br>
How would we know what we don’t know that we don’t know?<br>
<br>
Still working on the <a href="https://conwaylife.com/forums/viewtopic.php?f=2&amp;t=2286" class="postlink">(34,7)c/156 caterpillar</a> &lt;- HELP WANTED!</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123519" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123519">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2417" class="username">Chris857</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2417&amp;sr=posts">9</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> June 10th, 2020, 11:26 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123519">

						<h3 ><a href="#p123519">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123519" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123519#p123519" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2417" class="username">Chris857</a></strong> &raquo; </span>February 23rd, 2021, 1:01 am
			</p>
			
			
			
			<div class="content">#01 **<br>
#02 *<br>
#12 **<br>
#13 *<br>
#15 *<br>
#17 *<br>
#18 **<br>
#19 ***<br>
#22 *<br>
#24 **<br>
#26 **<br>
#27 *<br>
#28 *<br>
#30 **<br>
#32 *<br>
#33 *<br>
#34 ***<br>
#35 *<br>
#37 ***<br>
#38 ***<br>
#39 *</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
			

	<div class="action-bar bar-bottom">
	
			<a href="./posting.php?mode=reply&amp;f=2&amp;t=5066" class="button" title="Post a reply">
							<span>Post Reply</span> <i class="icon fa-reply fa-fw" aria-hidden="true"></i>
					</a>
		
		<div class="dropdown-container dropdown-button-control topic-tools">
		<span title="Topic tools" class="button button-secondary dropdown-trigger dropdown-select">
			<i class="icon fa-wrench fa-fw" aria-hidden="true"></i>
			<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
		</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																												<li>
					<a href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;view=print" title="Print view" accesskey="p">
						<i class="icon fa-print fa-fw" aria-hidden="true"></i><span>Print view</span>
					</a>
				</li>
											</ul>
		</div>
	</div>

			<form method="post" action="./viewtopic.php?f=2&amp;t=5066">
		<div class="dropdown-container dropdown-container-left dropdown-button-control sort-tools">
	<span title="Display and sorting options" class="button button-secondary dropdown-trigger dropdown-select">
		<i class="icon fa-sort-amount-asc fa-fw" aria-hidden="true"></i>
		<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
	</span>
	<div class="dropdown hidden">
		<div class="pointer"><div class="pointer-inner"></div></div>
		<div class="dropdown-contents">
			<fieldset class="display-options">
							<label>Display: <select name="st" id="st"><option value="0" selected="selected">All posts</option><option value="1">1 day</option><option value="7">7 days</option><option value="14">2 weeks</option><option value="30">1 month</option><option value="90">3 months</option><option value="180">6 months</option><option value="365">1 year</option></select></label>
								<label>Sort by: <select name="sk" id="sk"><option value="a">Author</option><option value="t" selected="selected">Post time</option><option value="s">Subject</option></select></label>
				<label>Direction: <select name="sd" id="sd"><option value="a" selected="selected">Ascending</option><option value="d">Descending</option></select></label>
								<hr class="dashed" />
				<input type="submit" class="button2" name="sort" value="Go" />
						</fieldset>
		</div>
	</div>
</div>
		</form>
	
	
	
			<div class="pagination">
			58 posts
							<ul>
		<li class="active"><span>1</span></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" role="button">2</a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=50" role="button">3</a></li>
				<li class="arrow next"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" rel="next" role="button"><i class="icon fa-chevron-right fa-fw" aria-hidden="true"></i><span class="sr-only">Next</span></a></li>
	</ul>
					</div>
	</div>


<div class="action-bar actions-jump">
		<p class="jumpbox-return">
		<a href="./viewforum.php?f=2" class="left-box arrow-left" accesskey="r">
			<i class="icon fa-angle-left fa-fw icon-black" aria-hidden="true"></i><span>Return to “Patterns”</span>
		</a>
	</p>
	
		<div class="jumpbox dropdown-container dropdown-container-right dropdown-up dropdown-left dropdown-button-control" id="jumpbox">
			<span title="Jump to" class="button button-secondary dropdown-trigger dropdown-select">
				<span>Jump to</span>
				<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
			</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																				<li><a href="./viewforum.php?f=6" class="jumpbox-cat-link"> <span> ConwayLife.com</span></a></li>
																<li><a href="./viewforum.php?f=3" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Website Discussion</span></a></li>
																<li><a href="./viewforum.php?f=4" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Bugs &amp; Errors</span></a></li>
																<li><a href="./viewforum.php?f=5" class="jumpbox-cat-link"> <span> Game of Life</span></a></li>
																<li><a href="./viewforum.php?f=7" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; General Discussion</span></a></li>
																<li><a href="./viewforum.php?f=11" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Other Cellular Automata</span></a></li>
																<li><a href="./viewforum.php?f=9" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Scripts</span></a></li>
																<li><a href="./viewforum.php?f=2" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Patterns</span></a></li>
																<li><a href="./viewforum.php?f=14" class="jumpbox-cat-link"> <span> The Sandbox</span></a></li>
																<li><a href="./viewforum.php?f=12" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; The Sandbox</span></a></li>
											</ul>
		</div>
	</div>

	</div>


			</div>


<div id="page-footer" class="page-footer" role="contentinfo">
	<div class="navbar" role="navigation">
	<div class="inner">

	<ul id="nav-footer" class="nav-footer linklist" role="menubar">
		<li class="breadcrumbs">
									<span class="crumb"><a href="./index.php" data-navbar-reference="index"><i class="icon fa-home fa-fw" aria-hidden="true"></i><span>Board index</span></a></span>					</li>
		
				<li class="rightside">All times are <span title="UTC-4">UTC-04:00</span></li>
							<li class="rightside">
				<a href="./ucp.php?mode=delete_cookies" data-ajax="true" data-refresh="true" role="menuitem">
					<i class="icon fa-trash fa-fw" aria-hidden="true"></i><span>Delete cookies</span>
				</a>
			</li>
																<li class="rightside" data-last-responsive="true">
				<a href="./memberlist.php?mode=contactadmin" role="menuitem">
					<i class="icon fa-envelope fa-fw" aria-hidden="true"></i><span>Contact us</span>
				</a>
			</li>
			</ul>

	</div>
</div>

	<div class="copyright">
				<p class="footer-row">
			<span class="footer-copyright">Powered by <a href="https://www.phpbb.com/">phpBB</a>&reg; Forum Software &copy; phpBB Limited</span>
		</p>
						<p class="footer-row">
			<a class="footer-link" href="./ucp.php?mode=privacy" title="Privacy" role="menuitem">
				<span class="footer-link-text">Privacy</span>
			</a>
			|
			<a class="footer-link" href="./ucp.php?mode=terms" title="Terms" role="menuitem">
				<span class="footer-link-text">Terms</span>
			</a>
			|
			<a class="footer-link" href="http://www.njohnston.ca" role="menuitem">
				<span class="footer-link-text">Created by Nathaniel Johnston</span>
			</a>
			|
			<a class="footer-link" href="https://platprices.com" role="menuitem">
				<span class="footer-link-text">Hosted by PlatPrices</span>
			</a>
		</p>
					</div>

	<div id="darkenwrapper" class="darkenwrapper" data-ajax-error-title="AJAX error" data-ajax-error-text="Something went wrong when processing your request." data-ajax-error-text-abort="User aborted request." data-ajax-error-text-timeout="Your request timed out; please try again." data-ajax-error-text-parsererror="Something went wrong with the request and the server returned an invalid reply.">
		<div id="darken" class="darken">&nbsp;</div>
	</div>

	<div id="phpbb_alert" class="phpbb_alert" data-l-err="Error" data-l-timeout-processing-req="Request timed out.">
		<a href="#" class="alert_close">
			<i class="icon fa-times-circle fa-fw" aria-hidden="true"></i>
		</a>
		<h3 class="alert_title">&nbsp;</h3><p class="alert_text"></p>
	</div>
	<div id="phpbb_confirm" class="phpbb_alert">
		<a href="#" class="alert_close">
			<i class="icon fa-times-circle fa-fw" aria-hidden="true"></i>
		</a>
		<div class="alert_text"></div>
	</div>
</div>

</div>

<div>
	<a id="bottom" class="anchor" accesskey="z"></a>
	<img src="./cron.php?cron_type=cron.task.core.prune_notifications" width="1" height="1" alt="cron" /></div>

<script src="./assets/javascript/jquery.min.js?assets_version=21"></script>
<script src="./assets/javascript/core.js?assets_version=21"></script>




<script src="./styles/prosilver/template/forum_fn.js?assets_version=21"></script>
<script src="./styles/prosilver/template/ajax.js?assets_version=21"></script>





</body>
</html>

<!DOCTYPE html>
<html dir="ltr" lang="en-gb">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<meta name="LifeViewer" content="rle code 37 hide limit">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>PotY 2020 Voting Thread - Page 2 - ConwayLife.com</title>


	<link rel="canonical" href="https://conwaylife.com/forums/viewtopic.php?t=5066&amp;start=25">

<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->

<style>
body{
	background-color:#FFF;
	background-image:url('https://www.conwaylife.com/images/back.png');
	background-position:center top;
}

#cl_navbar{font-size:150%;color:#222;position:relative;background-image:url('https://www.conwaylife.com/images/back_nav.png');background-position:center top;background-repeat:repeat-x;height:37px;margin-bottom:16px;margin-top:16px;line-height:37px;text-align:center;width:100%;border-right:1px solid #161646;border-left:1px solid #161646;}
#cl_navbar a{text-decoration:none;}
#cl_navbar a:hover{text-decoration:underline;}
</style>

<script type="text/javascript" src="./styles/prosilver/template/lv-plugin.js"></script>
<link href="./assets/css/font-awesome.min.css?assets_version=21" rel="stylesheet">
<link href="./styles/prosilver/theme/stylesheet.css?assets_version=21" rel="stylesheet">
<link href="./styles/prosilver/theme/en/stylesheet.css?assets_version=21" rel="stylesheet">




<!--[if lte IE 9]>
	<link href="./styles/prosilver/theme/tweaks.css?assets_version=21" rel="stylesheet">
<![endif]-->


<link href="./ext/phpbb/ads/styles/all/theme/phpbbads.css?assets_version=21" rel="stylesheet" media="screen" />


<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7884550756583428",
          enable_page_level_ads: true
     });
</script>

</head>
<body id="phpbb" class="nojs notouch section-viewtopic ltr ">


<div id="wrap" class="wrap">
	<a id="top" class="top-anchor" accesskey="t"></a>
	<div id="page-header">
		<div class="headerbar" role="banner">
					<div class="inner">

			<div id="site-description" class="site-description">
				<a id="logo" class="logo" href="./index.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Board index"><img src="../images/forumlogo.png" width="45" height="45" alt="ConwayLife.com - A community for Conway's Game of Life and related cellular automata" /></a>
				<h1>ConwayLife.com</h1>
				<p>Forums for Conway's Game of Life</p>
				<p class="skiplink"><a href="#start_here">Skip to content</a></p>
			</div>

									<div id="search-box" class="search-box search-header" role="search">
				<form action="./search.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" method="get" id="search">
				<fieldset>
					<input name="keywords" id="keywords" type="search" maxlength="128" title="Search for keywords" class="inputbox search tiny" size="20" value="" placeholder="Search…" />
					<button class="button button-search" type="submit" title="Search">
						<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
					</button>
					<a href="./search.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="button button-search-end" title="Advanced search">
						<i class="icon fa-cog fa-fw" aria-hidden="true"></i><span class="sr-only">Advanced search</span>
					</a>
					<input type="hidden" name="sid" value="fd333ae2713130fd66d7e2ea1ee4c9bc" />

				</fieldset>
				</form>
			</div>
						
			</div>



					</div>
								<div id="cl_navbar">
									<a href="../">Home</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<a href="/wiki/">LifeWiki</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<b><a href="http://www.conwaylife.com/forums/">Forums</a></b>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<!-- <a href="/applet/">Web Applet</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp; --><a href="http://golly.sourceforge.net/">Download Golly</a>
				</div>
				<div class="navbar" role="navigation">
	<div class="inner">

	<ul id="nav-main" class="nav-main linklist" role="menubar">

		<li id="quick-links" class="quick-links dropdown-container responsive-menu" data-skip-responsive="true">
			<a href="#" class="dropdown-trigger">
				<i class="icon fa-bars fa-fw" aria-hidden="true"></i><span>Quick links</span>
			</a>
			<div class="dropdown">
				<div class="pointer"><div class="pointer-inner"></div></div>
				<ul class="dropdown-contents" role="menu">
					
											<li class="separator"></li>
																									<li>
								<a href="./search.php?search_id=unanswered&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" role="menuitem">
									<i class="icon fa-file-o fa-fw icon-gray" aria-hidden="true"></i><span>Unanswered topics</span>
								</a>
							</li>
							<li>
								<a href="./search.php?search_id=active_topics&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" role="menuitem">
									<i class="icon fa-file-o fa-fw icon-blue" aria-hidden="true"></i><span>Active topics</span>
								</a>
							</li>
							<li class="separator"></li>
							<li>
								<a href="./search.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" role="menuitem">
									<i class="icon fa-search fa-fw" aria-hidden="true"></i><span>Search</span>
								</a>
							</li>
					
										<li class="separator"></li>

									</ul>
			</div>
		</li>

				<li data-skip-responsive="true">
			<a href="/forums/app.php/help/faq?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" rel="help" title="Frequently Asked Questions" role="menuitem">
				<i class="icon fa-question-circle fa-fw" aria-hidden="true"></i><span>FAQ</span>
			</a>
		</li>
						
			<li class="rightside"  data-skip-responsive="true">
			<a href="./ucp.php?mode=login&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Login" accesskey="x" role="menuitem">
				<i class="icon fa-power-off fa-fw" aria-hidden="true"></i><span>Login</span>
			</a>
		</li>
					<li class="rightside" data-skip-responsive="true">
				<a href="./ucp.php?mode=register&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" role="menuitem">
					<i class="icon fa-pencil-square-o  fa-fw" aria-hidden="true"></i><span>Register</span>
				</a>
			</li>
						</ul>

	<ul id="nav-breadcrumbs" class="nav-breadcrumbs linklist navlinks" role="menubar">
								<li class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
										<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope><a href="./index.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" itemtype="https://schema.org/Thing" itemprop="item" accesskey="h" data-navbar-reference="index"><i class="icon fa-home fa-fw"></i><span itemprop="name">Board index</span></a><meta itemprop="position" content="1" /></span>

											<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope data-forum-id="5"><a href="./viewforum.php?f=5&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" itemtype="https://schema.org/Thing" itemprop="item"><span itemprop="name">Game of Life</span></a><meta itemprop="position" content="2" /></span>
															<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope data-forum-id="2"><a href="./viewforum.php?f=2&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" itemtype="https://schema.org/Thing" itemprop="item"><span itemprop="name">Patterns</span></a><meta itemprop="position" content="3" /></span>
												</li>
		
					<li class="rightside responsive-search">
				<a href="./search.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="View the advanced search options" role="menuitem">
					<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
				</a>
			</li>
			</ul>

	</div>
</div>
	</div>



	
	<a id="start_here" class="anchor"></a>
	<div id="page-body" class="page-body" role="main">
		
		
<h2 class="topic-title"><a href="./viewtopic.php?f=2&amp;t=5066&amp;start=25&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">PotY 2020 Voting Thread</a></h2>
<!-- NOTE: remove the style="display: none" when you want to have the forum description on the topic body -->
<div style="display: none !important;">For discussion of specific patterns or specific families of patterns, both newly-discovered and well-known.<br /></div>


<div class="action-bar bar-top">
	
			<a href="./posting.php?mode=reply&amp;f=2&amp;t=5066&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="button" title="Post a reply">
							<span>Post Reply</span> <i class="icon fa-reply fa-fw" aria-hidden="true"></i>
					</a>
	
			<div class="dropdown-container dropdown-button-control topic-tools">
		<span title="Topic tools" class="button button-secondary dropdown-trigger dropdown-select">
			<i class="icon fa-wrench fa-fw" aria-hidden="true"></i>
			<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
		</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																												<li>
					<a href="./viewtopic.php?f=2&amp;t=5066&amp;start=25&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc&amp;view=print" title="Print view" accesskey="p">
						<i class="icon fa-print fa-fw" aria-hidden="true"></i><span>Print view</span>
					</a>
				</li>
											</ul>
		</div>
	</div>
	
			<div class="search-box" role="search">
			<form method="get" id="topic-search" action="./search.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc">
			<fieldset>
				<input class="inputbox search tiny"  type="search" name="keywords" id="search_keywords" size="20" placeholder="Search this topic…" />
				<button class="button button-search" type="submit" title="Search">
					<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
				</button>
				<a href="./search.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="button button-search-end" title="Advanced search">
					<i class="icon fa-cog fa-fw" aria-hidden="true"></i><span class="sr-only">Advanced search</span>
				</a>
				<input type="hidden" name="t" value="5066" />
<input type="hidden" name="sf" value="msgonly" />
<input type="hidden" name="sid" value="fd333ae2713130fd66d7e2ea1ee4c9bc" />

			</fieldset>
			</form>
		</div>
	
			<div class="pagination">
			58 posts
							<ul>
			<li class="arrow previous"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" rel="prev" role="button"><i class="icon fa-chevron-left fa-fw" aria-hidden="true"></i><span class="sr-only">Previous</span></a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" role="button">1</a></li>
			<li class="active"><span>2</span></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc&amp;start=50" role="button">3</a></li>
				<li class="arrow next"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc&amp;start=50" rel="next" role="button"><i class="icon fa-chevron-right fa-fw" aria-hidden="true"></i><span class="sr-only">Next</span></a></li>
	</ul>
					</div>
		</div>




			<div id="p123533" class="post has-profile bg2 online">
		<div class="inner">

		<dl class="postprofile" id="profile123533">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2322&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=2322_1611054705.gif" width="77" height="77" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2322&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">yujh</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2322&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">1927</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 27th, 2020, 11:23 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> CHINA</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content123533">

						<h3 class="first"><a href="#p123533">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123533&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123533&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123533" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2322&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">yujh</a></strong> &raquo; </span>February 23rd, 2021, 5:55 am
			</p>
			
			
			
			<div class="content">#01 ***<br>
#02 ***<br>
#03 **<br>
#04 **<br>
#05 **<br>
#06 *<br>
#07 ***<br>
#08 ***<br>
#09 ***<br>
#10 **<br>
#11 ***<br>
#12 ***<br>
#13 ***<br>
#14 ***<br>
#15 **<br>
#16 **<br>
#17 ***<br>
#18 **<br>
#19 ***<br>
#20 **<br>
#21 **<br>
#22 ***<br>
#23 ***<br>
#24 ***<br>
#25 *<br>
#26 ***<br>
#27 **<br>
#28 ***<br>
#29 ***<br>
#30 ***<br>
#31 ***<br>
#32 **<br>
#33 ***<br>
#34 ***<br>
#35 ***<br>
#36 **<br>
#37 ***<br>
#38 ***<br>
#39 ***<br>
Yayayayayay</div>

			
			
									
									<div id="sig123533" class="signature"><a href="https://www.conwaylife.com/forums/viewtopic.php?f=11&amp;t=4330" class="postlink">B34kz5e7c8/S23-a4ityz5k!!!</a><br>
<br>
<a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4890" class="postlink">b2n3-q5y6cn7s23-k4c8</a><br>
<br>
<a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4642" class="postlink">B3-kq6cn8/S2-i3-a4ciyz8</a><br>
<br>
<a href="https://conwaylife.com/wiki/User:Yujh" class="postlink">wiki</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
				
			<div id="p123589" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123589">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2150&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Pavgran</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2150&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">63</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> June 12th, 2019, 12:14 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content123589">

						<h3 ><a href="#p123589">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123589&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123589&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123589" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2150&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Pavgran</a></strong> &raquo; </span>February 24th, 2021, 8:58 am
			</p>
			
			
			
			<div class="content">#02 **<br>
#03 **<br>
#05 *<br>
#06 *<br>
#07 **<br>
#08 *<br>
#09 *<br>
#10 ***<br>
#11 ***<br>
#12 ***<br>
#13 **<br>
#14 ***<br>
#15 **<br>
#16 **<br>
#19 ***<br>
#20 *<br>
#23 **<br>
#24 **<br>
#25 *<br>
#27 **<br>
#28 **<br>
#30 ***<br>
#31 *<br>
#32 **<br>
#33 ***<br>
#34 ***<br>
#35 **<br>
#39 **</div>

			
			
													<div class="notice">
					Last edited by <a href="./memberlist.php?mode=viewprofile&amp;u=2150&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Pavgran</a> on March 11th, 2021, 11:46 am, edited 1 time in total.
									</div>
			
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123592" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123592">
			<dt class="has-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=216&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=216_1513913351.gif" width="45" height="32" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=216&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" style="color: #00AA00;" class="username-coloured">dvgrn</a>							</dt>

						<dd class="profile-rank">Moderator</dd>			
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=216&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">7697</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> May 17th, 2009, 11:00 pm</dd>		
		
																<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Madison, WI</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact dvgrn">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact dvgrn</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://b3s23life.blogspot.com" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123592">

						<h3 ><a href="#p123592">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123592&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123592&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123592" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=216&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" style="color: #00AA00;" class="username-coloured">dvgrn</a></strong> &raquo; </span>February 24th, 2021, 10:30 am
			</p>
			
			
			
			<div class="content">#01 **<br>
#02 *<br>
#03 **<br>
#04 *<br>
#05 ***<br>
#06 ***<br>
#07 ***<br>
#11 ***<br>
#12 *<br>
#13 ***<br>
#14 **<br>
#15 *<br>
#16 **<br>
#19 ***<br>
#22 ***<br>
#23 **<br>
#24 ***<br>
#25 **<br>
#28 ***<br>
#29 *<br>
#30 ***<br>
#31 *<br>
#32 **<br>
#33 ***<br>
#34 ***<br>
#35 **<br>
#36 *<br>
#37 ***<br>
#38 **<br>
#39 ***</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123700" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile123700">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1613&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Naszvadi</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1613&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">766</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> May 7th, 2016, 8:53 am</dd>		
		
											
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact Naszvadi">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact Naszvadi</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://www.conwaylife.com/forums/search.php?keywords=&amp;terms=any&amp;author=Naszvadi&amp;sc=1&amp;sf=titleonly&amp;sr=topics&amp;sk=a&amp;sd=d&amp;st=0&amp;ch=0&amp;t=0&amp;submit=Search" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123700">

						<h3 ><a href="#p123700">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123700&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123700&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123700" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1613&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Naszvadi</a></strong> &raquo; </span>February 25th, 2021, 7:31 am
			</p>
			
			
			
			<div class="content">#01 *<br>
#02 ***<br>
#03 ***<br>
#04 ***<br>
#05 *<br>
#06 *<br>
#07 ***<br>
#11 ***<br>
#12 ***<br>
#13 **<br>
#14 ***<br>
#15 *<br>
#17 *<br>
#18 ***<br>
#19 ***<br>
#20 **<br>
#21 ***<br>
#22 **<br>
#23 *<br>
#24 *<br>
#25 ***<br>
#27 **<br>
#28 ***<br>
#29 ***<br>
#30 **<br>
#31 ***<br>
#32 ***<br>
#33 ***<br>
#34 ***<br>
#37 **<br>
#38 ***<br>
#39 ***</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p123794" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile123794">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2342&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Dylan Chen</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2342&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">69</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> March 27th, 2020, 8:07 am</dd>		
		
											
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact Dylan Chen">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact Dylan Chen</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://plus.google.com/+inspire" title="Google+" class="last-cell">
										<span class="contact-icon phpbb_googleplus-icon">Google+</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content123794">

						<h3 ><a href="#p123794">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=123794&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=123794&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123794" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2342&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Dylan Chen</a></strong> &raquo; </span>February 26th, 2021, 8:36 am
			</p>
			
			
			
			<div class="content">#01 **<br>
#02 ***<br>
#03 **<br>
#04 **<br>
#05 **<br>
#06 **<br>
#07 **<br>
#08 **<br>
#09 ***<br>
#10 **<br>
#11 **<br>
#12 ***<br>
#13 ***<br>
#14 **<br>
#15 **<br>
#16 **<br>
#17 **<br>
#18 **<br>
#19 **<br>
#20 **<br>
#21 **<br>
#22 **<br>
#23 **<br>
#24 ***<br>
#25 **<br>
#26 **<br>
#27 **<br>
#28 **<br>
#29 ***<br>
#30 ***<br>
#31 ***<br>
#32 **<br>
#33 ***<br>
#34 **<br>
#35 **<br>
#36 **<br>
#37 ***<br>
#38 **<br>
#39 ***</div>

			
			
									
									<div id="sig123794" class="signature"><strong class="text-strong">Tools should not be the limit. </strong><br>
       Whether your obstacle is a script, an stdin, or Linux environment computing resouces.<br>
       check <a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;p=121292#p121579" class="postlink">New rules</a> thread for help.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124285" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile124285">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=1223_1431292145.gif" width="88" height="112" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">simsim314</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1223&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">1769</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 10th, 2014, 1:27 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124285">

						<h3 ><a href="#p124285">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124285&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124285&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124285" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">simsim314</a></strong> &raquo; </span>March 2nd, 2021, 8:45 pm
			</p>
			
			
			
			<div class="content">#01 *<br>
#04 *<br>
#07 *<br>
#09 *<br>
#10 *<br>
#11 **<br>
#12 **<br>
#13 **<br>
#14 **<br>
#15 *<br>
#16 *<br>
#17 *<br>
#18 **<br>
#19 ***<br>
#20 *<br>
#21 *<br>
#24 *<br>
#25 **<br>
#26 **<br>
#27 *<br>
#28 **<br>
#29 *<br>
#30 **<br>
#32 **<br>
#33 **<br>
#34 ***<br>
#35 **<br>
#36 *<br>
#37 **<br>
#38 **<br>
#39 **<br>
<br>
@wwei47 - do you realize that voting this way is the same as not voting at all? The voting is done to estimate subjective preferences of cgol community, giving same vote to everyone express zero preference.</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124286" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile124286">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=1223_1431292145.gif" width="88" height="112" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">simsim314</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1223&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">1769</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 10th, 2014, 1:27 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124286">

						<h3 ><a href="#p124286">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124286&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124286&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124286" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">simsim314</a></strong> &raquo; </span>March 2nd, 2021, 8:48 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">wwei47</a> wrote: <a href="./viewtopic.php?p=123499&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123499" data-post-id="123499" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 7:53 pm</div></cite>
<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=123497&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123497" data-post-id="123497" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 7:34 pm</div></cite>
<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">wwei47</a> wrote: <a href="./viewtopic.php?p=123480&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p123480" data-post-id="123480" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">February 22nd, 2021, 5:33 pm</div></cite>
I'll give each pattern three stars.
</div></blockquote>
Doesn't that remove the point of voting?
</div></blockquote>
I guess so, but it takes a lot of effort to produce those patterns, and for that, I give them 3 stars.
</div></blockquote>

Different patterns take different amount of effort and have different amount of subjective impact on the importance you give them in the research of the field. Even if effort is involved there are projects that took days, weeks, months some took years maybe, they still deserve different vote.<br>
<br>
<strong class="text-strong">EDIT</strong> To explain what I mean, here is pattern of mine, I call it #40, how much stars does it deserves? <br>

<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 88, y = 51, rule = B3/S23
59b25o$51b8o25bo$46b5o34bo$42b4o40bo$38b4o45bo$35b3o49bo$32b3o52bo$30b
2o55bo$28b2o57bo$26b2o59bo$24b2o61bo$21b3o63bo$19b2o66bo$17b2o68bo$15b
2o69bo$14bo70bo$12b2o17b4o50bo$10b2o16b3o4b3o46bo$8b2o16b2o10b3o42bo$
7bo17bo15b3o38bo$6bo37b2o35bo$4b2o40b2o32bo$3bo44b2o29bo$2bo47b2o26bo$
bo50b2o23bo$bo52b2o20bo$o55b2o16b2o$58bo11b4o$59bo6b4o$60bo2b3o$60b3o$
57b3o2bo$54b3o5bo$51b3o9bo$48b3o12bo$45b3o16bo$42b3o19bo$39b3o22bo$34b
5o26bo$28b6o31bo$20b8o37bo$12b8o44bo$5b7o52bo$4bo58bo$4bo56b2o$5b2o52b
2o$7bo49b2o$8b2o45b2o$10b5o36b4o$15b17o15b4o$32b15o!
</code></pre></div></div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124330" class="post has-profile bg1 online">
		<div class="inner">

		<dl class="postprofile" id="profile124330">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">4094</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124330">

						<h3 ><a href="#p124330">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124330&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124330&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124330" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a></strong> &raquo; </span>March 3rd, 2021, 11:30 am
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">simsim314</a> wrote: <a href="./viewtopic.php?p=124286&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124286" data-post-id="124286" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 2nd, 2021, 8:48 pm</div></cite>Different patterns take different amount of effort and have different amount of subjective impact on the importance you give them in the research of the field. Even if effort is involved there are projects that took days, weeks, months some took years maybe, they still deserve different vote.</div></blockquote>
Your points are valid, but if wwei47 genuinely sees no difference between slightly helpful and very helpful or requiring a little effort and requiring a lot of effort, then it might be best to let the issue be. The idea of the voting thread is to decide what the community as a whole judges as the best pattern, so large differences in opinion should be represented more than slight differences in opinion. If wwei47 sees no significant difference between each pattern, giving each pattern three stars is better than attempting to expand the infinitesimal range of opinions to a scale from zero to three. (Of course, by this argument, not voting at all would be just as viable as an option.) Also, welcome back.<br>
By the way, since you seem to be the expert on making switch engines efficiently, I'm wondering whether you know of any ways to create a pair of two parallel glider-producing switch engines with seven or fewer gliders.</div>

			
			
									
									<div id="sig124330" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124336" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile124336">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=2614_1614092063.gif" width="100" height="125" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">wwei47</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2614&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">238</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 18th, 2021, 11:18 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124336">

						<h3 ><a href="#p124336">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124336&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124336&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124336" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">wwei47</a></strong> &raquo; </span>March 3rd, 2021, 12:07 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=124330&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124330" data-post-id="124330" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 11:30 am</div></cite>
Your points are valid, but if wwei47 genuinely sees no difference between slightly helpful and very helpful or requiring a little effort and requiring a lot of effort, then it might be best to let the issue be. The idea of the voting thread is to decide what the community as a whole judges as the best pattern, so large differences in opinion should be represented more than slight differences in opinion. If wwei47 sees no significant difference between each pattern, giving each pattern three stars is better than attempting to expand the infinitesimal range of opinions to a scale from zero to three. (Of course, by this argument, not voting at all would be just as viable as an option.) Also, welcome back.<br>
By the way, since you seem to be the expert on making switch engines efficiently, I'm wondering whether you know of any ways to create a pair of two parallel glider-producing switch engines with seven or fewer gliders.
</div></blockquote>
1. It's not easy to judge how much effort was put into a pattern.<br>
2. Try using the infinite growth 3G twice.  <span style="color:green"><strong class="text-strong">EDIT by dvgrn:</strong> link to <a href="https://www.conwaylife.com/forums/viewtopic.php?p=13988#p13988" class="postlink">infinite growth 3G GPSE</a>.</span><br>
Edit 2:<br>
3. I think this is my first post to be edited by a moderator! <img class="smilies" src="./images/smilies/icon_surprised.gif" width="15" height="15" alt=":o" title="Surprised"></div>

			
			
													<div class="notice">
					Last edited by <a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">wwei47</a> on March 3rd, 2021, 12:37 pm, edited 1 time in total.
									</div>
			
									<div id="sig124336" class="signature"><div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 19, y = 17, rule = Symbiosis
11.A$9.2A.A$3.A$2.3A3.A6.A$.A2.A3.2A5.A$A7.A.3A2.2A$A.3A4.A.4A.3A$.2A
2.B3.A4.A$B.B8.2A$13.A$12.2A$7.B3.2A4.2B$8.2A.A5.A$10.A3.A$9.2AB.B$8.
A2.2B4.B$8.B9.A!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124341" class="post has-profile bg1 online">
		<div class="inner">

		<dl class="postprofile" id="profile124341">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">4094</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124341">

						<h3 ><a href="#p124341">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124341&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124341&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124341" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a></strong> &raquo; </span>March 3rd, 2021, 12:33 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">wwei47</a> wrote: <a href="./viewtopic.php?p=124336&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124336" data-post-id="124336" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 12:07 pm</div></cite>2. Try using the infinite growth 3G twice.  <span style="color:green"><strong class="text-strong">EDIT by dvgrn:</strong> link to <a href="https://www.conwaylife.com/forums/viewtopic.php?p=13988#p13988" class="postlink">infinite growth 3G GPSE</a>.</span>
</div></blockquote>
It can't have escaping gliders.</div>

			
			
									
									<div id="sig124341" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124350" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile124350">
			<dt class="has-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=216&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=216_1513913351.gif" width="45" height="32" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=216&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" style="color: #00AA00;" class="username-coloured">dvgrn</a>							</dt>

						<dd class="profile-rank">Moderator</dd>			
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=216&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">7697</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> May 17th, 2009, 11:00 pm</dd>		
		
																<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Madison, WI</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact dvgrn">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact dvgrn</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://b3s23life.blogspot.com" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content124350">

						<h3 ><a href="#p124350">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124350&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124350&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124350" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=216&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" style="color: #00AA00;" class="username-coloured">dvgrn</a></strong> &raquo; </span>March 3rd, 2021, 1:45 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=124341&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124341" data-post-id="124341" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 12:33 pm</div></cite>It can't have escaping gliders.</div></blockquote>
Aha.  Any other requirements that didn't make it into the original request?  How far apart or close together do these parallel glider streams have to be, to be useful?<br>
<br>
It's easy-ish to write a search <span class="posthilit">script</span> to look for 7G no-extraneous-gliders double GPSE syntheses, but it might as well be written to look for the most useful solution first.  My idea would be to set up 3G switch engine syntheses in pairs at different distances and time offsets, and then see if there's a single glider impact in the debris that can convert both switch engines to GPSEs before the natural gliders start escaping.<br>
<br>
Luckily the first extraneous escaping glider only appears at T=583.  It's a huge search space.  ... Which should probably be discussed further in its own thread instead of here.</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124351" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile124351">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=1223_1431292145.gif" width="88" height="112" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">simsim314</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1223&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">1769</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 10th, 2014, 1:27 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124351">

						<h3 ><a href="#p124351">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124351&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124351&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124351" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1223&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">simsim314</a></strong> &raquo; </span>March 3rd, 2021, 1:59 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=124330&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124330" data-post-id="124330" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 11:30 am</div></cite>
large differences in opinion should be represented more than slight differences in opinion.
</div></blockquote>

As far as I understand the voting results is just sorted eventually.<br>

<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=124330&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124330" data-post-id="124330" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 11:30 am</div></cite>
If wwei47 sees no significant difference between each pattern, giving each pattern three stars is better than attempting to expand the infinitesimal range of opinions to a scale from zero to three. 
</div></blockquote>

If you agree that pattern #40 doesn't deserve 3 stars, then you still have some scoring function of how good some pattern is. Even this year people discovered more patterns than those 39, it's just those are of some significance chosen by the one who opened the post. Saying they all worth the same is basically the same as saying non of them worth anything. This year it's hard to find the knockout blow, pattern which way ahead of anything else done ever (like syringe or quadratic growth), but you can still find some patterns which shines more than others. I really don't want to express my subjective judgement here, but even one pattern that gets something else than 3 stars would be an input of significance.<br>

<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">wwei47</a> wrote: <a href="./viewtopic.php?p=124336&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124336" data-post-id="124336" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 12:07 pm</div></cite>
1. It's not easy to judge how much effort was put into a pattern.
</div></blockquote>

I agree but some are clearly were done with much more effort than others. Even rough estimation of the effort or slight correlation with effort by how much effort you believe was put into them, is still a meaningful input. Not being able to exactly estimate the effort doesn't mean they all the same. Giving three stars to them all doesn't influence the end result in any way, if you want people to invest more effort, then having them sorted out by estimated effort put into them - would be a meaningful voting even if not completely accurate.<br>
<br>
You can also consult an expert like dvgrn to estimate the effort. <br>

<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=124330&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124330" data-post-id="124330" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 11:30 am</div></cite>
I'm wondering whether you know of any ways to create a pair of two parallel glider-producing switch engines with seven or fewer gliders.
</div></blockquote>

There is a difference if you want them clean or messy. For messy and 4 gliders there is R+glider that produces glider producing SE, as of 7 gliders I would take one escaping glider for the second one. This will limit the output positions of course.</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124352" class="post has-profile bg2 online">
		<div class="inner">

		<dl class="postprofile" id="profile124352">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">4094</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124352">

						<h3 ><a href="#p124352">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124352&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124352&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124352" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a></strong> &raquo; </span>March 3rd, 2021, 2:19 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=216&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">dvgrn</a> wrote: <a href="./viewtopic.php?p=124350&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124350" data-post-id="124350" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 1:45 pm</div></cite>
<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=124341&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124341" data-post-id="124341" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 3rd, 2021, 12:33 pm</div></cite>It can't have escaping gliders.</div></blockquote>
Aha.  Any other requirements that didn't make it into the original request?  How far apart or close together do these parallel glider streams have to be, to be useful?</div></blockquote>
Here's the GPSE pair that the seventeen-glider RCT-based universal constructor uses:
<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 568, y = 591, rule = B3/S23
566bo$566b2o$565bobo21$549b2o$550b2o$549bo39$502bo$502b2o$501bobo21$485b
2o$486b2o$485bo39$438bo$438b2o$437bobo21$421b2o$422b2o$421bo39$374bo$
374b2o$373bobo21$357b2o$358b2o$357bo39$310bo$310b2o$309bobo21$293b2o$
294b2o$293bo38$169bo$162bo5bobo75bo$160b2ob2o81b2o$160b2o3bo2b3o74bob
o$160b2ob2o$161b4o$149b2o$149b2o7$141b2o$141b2o$155b3o$153b2ob4o4b4o$
153bo2b2o4b2obobo$145b2o6b3o9b2o$144bo2bo10bobo3bo$144bo2bo$145bobo$147b
4o$146b2o2bo78b2o$134b2o11b3o80b2o$133bobo12bo12b2o66bo$134bo26b2o$142b
2o5bobo$142b2o5bo$152bo$147bo4b2o$146bo6bo$146bo2b4o5$117b2o$100b2o3b
o11b2o$101bo4bo$105bo$102bobo24bo$103bo24bobo$107bo21b2o12bo$91b2o11b
3obo33bobo$91b2o10b3o2bo34b2o$102b2o2bo$103b2o$104bo$134bo$91b3o39bob
o$90b2ob3o38b2o$86bo$86bo3bo$86bo7b2o$91b2obo$93b2o$94bo6b2o$101b2o$98b
2ob2o$99b2o$93b2o9bo$92bo2bo7b3o27b2o$93b2o8b2obo25bo2bo$105b2o26b2o47b
o$182b2o$136b3o42bobo2$92b2o40bo$94bo39bo$89bo4b2o38bo$88bo3bo5bo$89b
2o7b2o31b2o$98b2o31b2o2$65b2o44bo$64bo2bo42bobo$65b2o44b2o$93b3o$91b2o
b2o$91b2ob2o$91b2o9bo$101bobo$102b2o2$139b2o$72bo66b2o$71bobo91b2o$70b
o2bo92b2o$71b2o92bo4$143b2o$101b2o40b2o$100bo2bo$101b2o2$104b3o2$102b
o126bobo$102bo126bo2bo$102bo126bobo$133b2o79b2o$99b2o32bobo77bobo$99b
2o33b2o77b3o13bo$88b3o121bo2bo11b2obo$33b2o179bo12b2obo$32bo2bo169b2o
7bo12bo3bo$33b2o170b2o20b4o$81bo130bo15b3o$81bo133bo$81bo130bo2bo$70b
o143b3o$69bobo142b3o$70b2o141bo2bo$213bobo$107b2o91b2o11b3o$40bo66b2o
90bob2o$39bobo157bo4bo$38bo2bo157bo8b2o15b2o$39b2o158bo3b2o2bobo15b2o
$206b2obo$201bo5b6o$199b2o7bo4bo$111b2o97bo2bo$69b2o40b2o24b2o47b2o23b
2o$68bo2bo65b2o47bobo14b2o$69b2o116bo15b2o12b2o$217b2o$72b3o70b2o$145b
2o$70bo94bo$70bo94bo$70bo94bo$101b2o23b2o40bo11b2o24b2o$67b2o32bobo22b
obo32b3o3bobo10bobo22bobo$67b2o33b2o23b2o38bobo11bo24bo$56b3o99b2o8bo
$b2o155b2o$o2bo184b2o$b2o185bobo$49bo139bo$49bo$49bo$38bo$37bobo$38b2o
2$75b2o$8bo66b2o$7bobo$6bo2bo183b2o$7b2o184b2o4$79b2o$37b2o40b2o24b2o
47b2o$36bo2bo65b2o47bobo$37b2o116bo2$40b3o70b2o71b2o$113b2o73bo$38bo94b
o33b2o16bobo4bo$38bo94bo33bobo16b2o3bobo$38bo94bo34b3o12b2ob2o2bo$69b
2o23b2o40bo11b2o18b2o14b3o2bo$35b2o32bobo22bobo32b3o3bobo10bobo21b3o10b
o4bo$35b2o33b2o23b2o38bobo11bo22b3o17b2o$24b3o99b2o8bo34bo21bo$126b2o
30b2o11b2o$158b3o11bo2$17bo$17bo133b3o7bo7bo7bo2bo$17bo133b3o7b2o5bob
o3b4o2bo$6bo146bo9bo4bobo4bo4b2o$5bobo142b3o7bo2bo5bo6bo4bo4b2o$6b2o142b
3o23bo3b2o4b2o$125bo33bo18bobo$43b2o80bo33bobo$43b2o80bo34bo2$177b3o5$
47b2o70b3o$5b2o40b2o24b2o$4bo2bo65b2o$5b2o2$8b3o70b2o$81b2o$6bo94bo$6b
o94bo41b2o$6bo94bo40bo2bo$37b2o23b2o40bo11b2o24bobo$3b2o32bobo22bobo32b
3o3bobo10bobo24bo$3b2o33b2o23b2o38bobo11bo$94b2o8bo$94b2o59bo$154bobo
$154bobo$155bo6$93bo$93bo$93bo7$87b3o$41b2o$41b2o3$49b2o$49b2o$69bo$69b
o41b2o$69bo40bo2bo$30b2o40bo11b2o24bobo$30bobo32b3o3bobo10bobo24bo$31b
2o38bobo11bo$62b2o8bo$62b2o59bo$122bobo$122bobo$123bo6$61bo$61bo$61bo
7$55b3o7$37bo$37bo41b2o$37bo40bo2bo$40bo11b2o24bobo$33b3o3bobo10bobo24b
o$39bobo11bo$30b2o8bo$30b2o59bo$90bobo$90bobo$91bo!</code></pre></div>
What matters is the glider stream. Changes that don't affect the glider stream, such as having one GPSE create each glider one full diagonal farther back but four generations sooner, will not cause problems. Also, some changes to the glider stream will be allowed. In order to keep using the same model, the lane difference must be kept the same, the relative timings of the two gliders modulo eight must be kept the same, and gliders from the two parallel GPSEs can't come too close together to each other, but changes that don't violate any of those should be fine. Also, if the lane separation and or modular relative timing are changed, then the current model won't work, but it will probably be possible to create a new model as long as the lane separation and relative parity are universal (which I would expect imposes a maximum lane separation) and the gliders aren't too close together timewise, so the only stringent conditions are no escaping gliders, universality, and sufficient temporal separation between when gliders from the two parallel GPSEs arrive.<br>
<br>
<br>
<br>
Edit: Actually, if the first glider from the GPSE on the southeast arrives before the the first glider from the GPSE on the northwest or after the second glider from the GPSE on the northwest, that might cause a minor hassle, but I'm sure that there would be a way to make it work without having to overhaul the design find an entirely new set of universal operations, so we needn't worry about it.</div>

			
			
									
									<div id="sig124352" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124689" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile124689">
			<dt class="has-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=343&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" style="color: #00AA00;" class="username-coloured">Sokwe</a>							</dt>

						<dd class="profile-rank">Moderator</dd>			
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=343&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">1785</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> July 9th, 2009, 2:44 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124689">

						<h3 ><a href="#p124689">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124689&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124689&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124689" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=343&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" style="color: #00AA00;" class="username-coloured">Sokwe</a></strong> &raquo; </span>March 6th, 2021, 9:05 pm
			</p>
			
			
			
			<div class="content">#01 **<br>
#02 **<br>
#03 **<br>
#04 *<br>
#05 **<br>
#06 *<br>
#07 **<br>
#08 *<br>
#09 *<br>
#10 **<br>
#11 *<br>
#12 ***<br>
#13 **<br>
#14 ***<br>
#15 **<br>
#16 *<br>
#17 *<br>
#18 **<br>
#19 ***<br>
#20 *<br>
#21 **<br>
#22 ***<br>
#23 *<br>
#24 **<br>
#25 *<br>
#26 *<br>
#27 *<br>
#28 **<br>
#29 *<br>
#30 ***<br>
#31 **<br>
#32 *<br>
#33 **<br>
#34 **<br>
#35 *<br>
#36 *<br>
#37 **<br>
#38 ***<br>
#39 ***<br>
<br>
We still need to decide on an end date for the voting period. Last year the voting period lasted a little over 3 weeks. To have a similar voting period this year, we would end on March 17, a Wednesday. Is there a preferred day of the week to end on? Last year we ended on a Sunday.</div>

			
			
									
									<div id="sig124689" class="signature">-Matthias Merzenich</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124691" class="post has-profile bg2 online">
		<div class="inner">

		<dl class="postprofile" id="profile124691">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">4094</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124691">

						<h3 ><a href="#p124691">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124691&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124691&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124691" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a></strong> &raquo; </span>March 6th, 2021, 9:13 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=343&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">Sokwe</a> wrote: <a href="./viewtopic.php?p=124689&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124689" data-post-id="124689" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 6th, 2021, 9:05 pm</div></cite>Is there a preferred day of the week to end on? Last year we ended on a Sunday.
</div></blockquote>
I was planning to end the voting now but then saw that you voted. Shall the voting end anyway?</div>

			
			
									
									<div id="sig124691" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124693" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile124693">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1306&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=1306_1589928695.gif" width="97" height="97" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1306&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">A for awesome</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1306&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">2333</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> September 13th, 2014, 5:36 pm</dd>		
		
																<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Pembina University, Home of the Gliders</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact A for awesome">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact A for awesome</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://www.example.com/" title="Website" class="last-cell">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content124693">

						<h3 ><a href="#p124693">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124693&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124693&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124693" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1306&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">A for awesome</a></strong> &raquo; </span>March 6th, 2021, 10:11 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">MathAndCode</a> wrote: <a href="./viewtopic.php?p=124691&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124691" data-post-id="124691" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 6th, 2021, 9:13 pm</div></cite>I was planning to end the voting now but then saw that you voted. Shall the voting end anyway?
</div></blockquote>

I think it's a bad idea to end voting without announcing when voting will end well in advance. My suggestion would be one of three options:
<ol style="list-style-type:decimal"><li>End voting a week from today,</li>
<li>End voting on Pi Day (the 14th),<br>
Or my personal favorite:</li> <li>End voting on CGoL Day (the 23rd).</li></ol></div>

			
			
									
									<div id="sig124693" class="signature">praosylen#5847 (Discord)<br>
<br>
The only decision I made was made<br>
of flowers, to jump universes to one of springtime in<br>
a land of former winter, where no invisible walls stood,<br>
or could stand for more than a few hours at most...</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124694" class="post has-profile bg2 online">
		<div class="inner">

		<dl class="postprofile" id="profile124694">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">4094</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124694">

						<h3 ><a href="#p124694">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124694&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124694&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124694" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a></strong> &raquo; </span>March 6th, 2021, 10:14 pm
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=1306&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">A for awesome</a> wrote: <a href="./viewtopic.php?p=124693&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124693" data-post-id="124693" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 6th, 2021, 10:11 pm</div></cite>I think it's a bad idea to end voting without announcing when voting will end well in advance.</div></blockquote>
I'm going with Sokwe's suggestion of ending on March 17th.</div>

			
			
									
									<div id="sig124694" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124789" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile124789">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=762_1575276963.gif" width="41" height="41" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">otismo</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=762&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">640</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 18th, 2010, 1:41 pm</dd>		
		
																					<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Florida</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact otismo">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact otismo</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://foxeo.com" title="Website">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																																																<a href="ymsgr:sendim?foxeo@ymail.com" title="Yahoo Messenger" class="last-cell">
										<span class="contact-icon phpbb_yahoo-icon">Yahoo Messenger</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content124789">

						<h3 ><a href="#p124789">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124789&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124789&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124789" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">otismo</a></strong> &raquo; </span>March 7th, 2021, 10:17 pm
			</p>
			
			
			
			<div class="content">only 25 people voted - pitiful turnout...</div>

			
			
									
									<div id="sig124789" class="signature">In the Game of Life there are no winners there are no losers there are only SURVIVORS.<br>
.....**<br>
...****<br>
.******<br>
*( ͡° ͜ʖ ͡°)*
<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>#CXRLE Pos=0,0
x = 26, y = 34, rule = B3/S23
23bo$23bobo$23b2o9$2b3o2$o5bo$o5bo$o5bo2$2b3o5b2o$10b2o3$2b2o$2b2o10$
15b2o$15b2o!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124805" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile124805">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2142&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=2142_1566627510.gif" width="114" height="114" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2142&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">PC101</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2142&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">124</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> May 9th, 2019, 11:32 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> :noiɟɒɔo⅃</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content124805">

						<h3 ><a href="#p124805">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124805&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124805&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124805" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2142&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">PC101</a></strong> &raquo; </span>March 8th, 2021, 1:43 am
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">otismo</a> wrote: <a href="./viewtopic.php?p=124789&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124789" data-post-id="124789" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 7th, 2021, 10:17 pm</div></cite>
only 25 people voted - pitiful turnout...
</div></blockquote>

About 25 people votes last year as well.</div>

			
			
									
									<div id="sig124805" class="signature">Puffer Suppressor<br>
Would we be able to know when we know everything there is to know?<br>
How would we know what we don’t know that we don’t know?<br>
<br>
Still working on the <a href="https://conwaylife.com/forums/viewtopic.php?f=2&amp;t=2286" class="postlink">(34,7)c/156 caterpillar</a> &lt;- HELP WANTED!</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124808" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile124808">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=141&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=141_1504612401.jpg" width="64" height="64" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=141&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Macbi</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=141&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">792</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> March 29th, 2009, 4:58 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content124808">

						<h3 ><a href="#p124808">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124808&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124808&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124808" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=141&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Macbi</a></strong> &raquo; </span>March 8th, 2021, 4:27 am
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2142&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">PC101</a> wrote: <a href="./viewtopic.php?p=124805&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124805" data-post-id="124805" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 8th, 2021, 1:43 am</div></cite>
<blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">otismo</a> wrote: <a href="./viewtopic.php?p=124789&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124789" data-post-id="124789" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">March 7th, 2021, 10:17 pm</div></cite>
only 25 people voted - pitiful turnout...
</div></blockquote>

About 25 people votes last year as well.
</div></blockquote>I did some quick counts:
<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>2010 16
2011 19
2015 22
2016 23
2017 20
2018 37
2019 25</code></pre></div>
So more than 25 people this year would actually be quite good, except in comparison to 2018 which was a bumper year (likely because Sir Robin attracted lots of people).</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124823" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile124823">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=762_1575276963.gif" width="41" height="41" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">otismo</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=762&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">640</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 18th, 2010, 1:41 pm</dd>		
		
																					<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Florida</dd>
							
							<dd class="profile-contact">
				<strong>Contact:</strong>
				<div class="dropdown-container dropdown-left">
					<a href="#" class="dropdown-trigger" title="Contact otismo">
						<i class="icon fa-commenting-o fa-fw icon-lg" aria-hidden="true"></i><span class="sr-only">Contact otismo</span>
					</a>
					<div class="dropdown">
						<div class="pointer"><div class="pointer-inner"></div></div>
						<div class="dropdown-contents contact-icons">
																																								<div>
																	<a href="http://foxeo.com" title="Website">
										<span class="contact-icon phpbb_website-icon">Website</span>
									</a>
																																																<a href="ymsgr:sendim?foxeo@ymail.com" title="Yahoo Messenger" class="last-cell">
										<span class="contact-icon phpbb_yahoo-icon">Yahoo Messenger</span>
									</a>
																	</div>
																					</div>
					</div>
				</div>
			</dd>
				
		</dl>

		<div class="postbody">
						<div id="post_content124823">

						<h3 ><a href="#p124823">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124823&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124823&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124823" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=762&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">otismo</a></strong> &raquo; </span>March 8th, 2021, 11:57 am
			</p>
			
			
			
			<div class="content">hmmm...more candidates than voters - do not know what to make of it...</div>

			
			
									
									<div id="sig124823" class="signature">In the Game of Life there are no winners there are no losers there are only SURVIVORS.<br>
.....**<br>
...****<br>
.******<br>
*( ͡° ͜ʖ ͡°)*
<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>#CXRLE Pos=0,0
x = 26, y = 34, rule = B3/S23
23bo$23bobo$23b2o9$2b3o2$o5bo$o5bo$o5bo2$2b3o5b2o$10b2o3$2b2o$2b2o10$
15b2o$15b2o!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p124909" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile124909">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2480&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Anonymous Glider</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2480&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">5</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 28th, 2020, 12:06 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Somewhere (probably)</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content124909">

						<h3 ><a href="#p124909">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=124909&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=124909&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p124909" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2480&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">Anonymous Glider</a></strong> &raquo; </span>March 9th, 2021, 12:25 pm
			</p>
			
			
			
			<div class="content">#01 **<br>
#02 *<br>
#03 *<br>
#09 *<br>
#11 **<br>
#14 **<br>
#16 *<br>
#19 **<br>
#26 *<br>
#27 **<br>
#28 ***<br>
#30 **<br>
#31 **<br>
#32 **<br>
#33 **<br>
#34 ***<br>
#35 **<br>
#39 **</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p125076" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile125076">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2626&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=2626_1615490342.gif" width="126" height="98" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2626&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">cgoler2</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2626&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">11</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> March 10th, 2021, 2:32 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content125076">

						<h3 ><a href="#p125076">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125076&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125076&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p125076" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2626&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">cgoler2</a></strong> &raquo; </span>March 10th, 2021, 4:01 pm
			</p>
			
			
			
			<div class="content">#2 **<br>
#4 *<br>
#9 **<br>
#12 **<br>
#16 **<br>
#19 ***<br>
#24 **<br>
#30 *<br>
#33 ***<br>
#34 ***<br>
#37 *<br>
#38 **<br>
#39 **</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p125564" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile125564">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2238&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="avatar"><img class="avatar" src="./download/file.php?avatar=2238_1602743450.gif" width="100" height="50" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2238&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">JP21</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2238&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">1382</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> October 26th, 2019, 5:39 am</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> PH</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content125564">

						<h3 ><a href="#p125564">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125564&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125564&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p125564" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2238&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">JP21</a></strong> &raquo; </span>March 14th, 2021, 10:02 am
			</p>
			
			
			
			<div class="content">#01 *<br>
#03 *<br>
#04 *<br>
#05 **<br>
#07 *<br>
#09 ***<br>
#10 *<br>
#12 **<br>
#14 **<br>
#15 *<br>
#16 *<br>
#17 *<br>
#18 *<br>
#19 **<br>
#20 *<br>
#21 **<br>
#22 **<br>
#23 *<br>
#24 ***<br>
#27 *<br>
#28 *<br>
#29 **<br>
#30 *<br>
#31 **<br>
#33 ***<br>
#34 **<br>
#35 **<br>
#36 *<br>
#37 ***<br>
#38 **<br>
#39 *</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p125788" class="post has-profile bg2 online">
		<div class="inner">

		<dl class="postprofile" id="profile125788">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">4094</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content125788">

						<h3 ><a href="#p125788">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125788&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125788&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc#p125788" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="username">MathAndCode</a></strong> &raquo; </span>March 15th, 2021, 10:06 pm
			</p>
			
			
			
			<div class="content">This is just a reminder that the voting ends in two days (plus or minus a few hours).</div>

			
			
									
									<div id="sig125788" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
			

	<div class="action-bar bar-bottom">
	
			<a href="./posting.php?mode=reply&amp;f=2&amp;t=5066&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="button" title="Post a reply">
							<span>Post Reply</span> <i class="icon fa-reply fa-fw" aria-hidden="true"></i>
					</a>
		
		<div class="dropdown-container dropdown-button-control topic-tools">
		<span title="Topic tools" class="button button-secondary dropdown-trigger dropdown-select">
			<i class="icon fa-wrench fa-fw" aria-hidden="true"></i>
			<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
		</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																												<li>
					<a href="./viewtopic.php?f=2&amp;t=5066&amp;start=25&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc&amp;view=print" title="Print view" accesskey="p">
						<i class="icon fa-print fa-fw" aria-hidden="true"></i><span>Print view</span>
					</a>
				</li>
											</ul>
		</div>
	</div>

			<form method="post" action="./viewtopic.php?f=2&amp;t=5066&amp;start=25&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc">
		<div class="dropdown-container dropdown-container-left dropdown-button-control sort-tools">
	<span title="Display and sorting options" class="button button-secondary dropdown-trigger dropdown-select">
		<i class="icon fa-sort-amount-asc fa-fw" aria-hidden="true"></i>
		<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
	</span>
	<div class="dropdown hidden">
		<div class="pointer"><div class="pointer-inner"></div></div>
		<div class="dropdown-contents">
			<fieldset class="display-options">
							<label>Display: <select name="st" id="st"><option value="0" selected="selected">All posts</option><option value="1">1 day</option><option value="7">7 days</option><option value="14">2 weeks</option><option value="30">1 month</option><option value="90">3 months</option><option value="180">6 months</option><option value="365">1 year</option></select></label>
								<label>Sort by: <select name="sk" id="sk"><option value="a">Author</option><option value="t" selected="selected">Post time</option><option value="s">Subject</option></select></label>
				<label>Direction: <select name="sd" id="sd"><option value="a" selected="selected">Ascending</option><option value="d">Descending</option></select></label>
								<hr class="dashed" />
				<input type="submit" class="button2" name="sort" value="Go" />
						</fieldset>
		</div>
	</div>
</div>
		</form>
	
	
	
			<div class="pagination">
			58 posts
							<ul>
			<li class="arrow previous"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" rel="prev" role="button"><i class="icon fa-chevron-left fa-fw" aria-hidden="true"></i><span class="sr-only">Previous</span></a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" role="button">1</a></li>
			<li class="active"><span>2</span></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc&amp;start=50" role="button">3</a></li>
				<li class="arrow next"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc&amp;start=50" rel="next" role="button"><i class="icon fa-chevron-right fa-fw" aria-hidden="true"></i><span class="sr-only">Next</span></a></li>
	</ul>
					</div>
	</div>


<div class="action-bar actions-jump">
		<p class="jumpbox-return">
		<a href="./viewforum.php?f=2&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="left-box arrow-left" accesskey="r">
			<i class="icon fa-angle-left fa-fw icon-black" aria-hidden="true"></i><span>Return to “Patterns”</span>
		</a>
	</p>
	
		<div class="jumpbox dropdown-container dropdown-container-right dropdown-up dropdown-left dropdown-button-control" id="jumpbox">
			<span title="Jump to" class="button button-secondary dropdown-trigger dropdown-select">
				<span>Jump to</span>
				<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
			</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																				<li><a href="./viewforum.php?f=6&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-cat-link"> <span> ConwayLife.com</span></a></li>
																<li><a href="./viewforum.php?f=3&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Website Discussion</span></a></li>
																<li><a href="./viewforum.php?f=4&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Bugs &amp; Errors</span></a></li>
																<li><a href="./viewforum.php?f=5&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-cat-link"> <span> Game of Life</span></a></li>
																<li><a href="./viewforum.php?f=7&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; General Discussion</span></a></li>
																<li><a href="./viewforum.php?f=11&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Other Cellular Automata</span></a></li>
																<li><a href="./viewforum.php?f=9&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Scripts</span></a></li>
																<li><a href="./viewforum.php?f=2&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Patterns</span></a></li>
																<li><a href="./viewforum.php?f=14&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-cat-link"> <span> The Sandbox</span></a></li>
																<li><a href="./viewforum.php?f=12&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; The Sandbox</span></a></li>
											</ul>
		</div>
	</div>

	</div>


			</div>


<div id="page-footer" class="page-footer" role="contentinfo">
	<div class="navbar" role="navigation">
	<div class="inner">

	<ul id="nav-footer" class="nav-footer linklist" role="menubar">
		<li class="breadcrumbs">
									<span class="crumb"><a href="./index.php?sid=fd333ae2713130fd66d7e2ea1ee4c9bc" data-navbar-reference="index"><i class="icon fa-home fa-fw" aria-hidden="true"></i><span>Board index</span></a></span>					</li>
		
				<li class="rightside">All times are <span title="UTC-4">UTC-04:00</span></li>
							<li class="rightside">
				<a href="./ucp.php?mode=delete_cookies&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" data-ajax="true" data-refresh="true" role="menuitem">
					<i class="icon fa-trash fa-fw" aria-hidden="true"></i><span>Delete cookies</span>
				</a>
			</li>
																<li class="rightside" data-last-responsive="true">
				<a href="./memberlist.php?mode=contactadmin&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" role="menuitem">
					<i class="icon fa-envelope fa-fw" aria-hidden="true"></i><span>Contact us</span>
				</a>
			</li>
			</ul>

	</div>
</div>

	<div class="copyright">
				<p class="footer-row">
			<span class="footer-copyright">Powered by <a href="https://www.phpbb.com/">phpBB</a>&reg; Forum Software &copy; phpBB Limited</span>
		</p>
						<p class="footer-row">
			<a class="footer-link" href="./ucp.php?mode=privacy&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Privacy" role="menuitem">
				<span class="footer-link-text">Privacy</span>
			</a>
			|
			<a class="footer-link" href="./ucp.php?mode=terms&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" title="Terms" role="menuitem">
				<span class="footer-link-text">Terms</span>
			</a>
			|
			<a class="footer-link" href="http://www.njohnston.ca" role="menuitem">
				<span class="footer-link-text">Created by Nathaniel Johnston</span>
			</a>
			|
			<a class="footer-link" href="https://platprices.com" role="menuitem">
				<span class="footer-link-text">Hosted by PlatPrices</span>
			</a>
		</p>
					</div>

	<div id="darkenwrapper" class="darkenwrapper" data-ajax-error-title="AJAX error" data-ajax-error-text="Something went wrong when processing your request." data-ajax-error-text-abort="User aborted request." data-ajax-error-text-timeout="Your request timed out; please try again." data-ajax-error-text-parsererror="Something went wrong with the request and the server returned an invalid reply.">
		<div id="darken" class="darken">&nbsp;</div>
	</div>

	<div id="phpbb_alert" class="phpbb_alert" data-l-err="Error" data-l-timeout-processing-req="Request timed out.">
		<a href="#" class="alert_close">
			<i class="icon fa-times-circle fa-fw" aria-hidden="true"></i>
		</a>
		<h3 class="alert_title">&nbsp;</h3><p class="alert_text"></p>
	</div>
	<div id="phpbb_confirm" class="phpbb_alert">
		<a href="#" class="alert_close">
			<i class="icon fa-times-circle fa-fw" aria-hidden="true"></i>
		</a>
		<div class="alert_text"></div>
	</div>
</div>

</div>

<div>
	<a id="bottom" class="anchor" accesskey="z"></a>
	<img src="./cron.php?cron_type=cron.task.core.prune_notifications&amp;sid=fd333ae2713130fd66d7e2ea1ee4c9bc" width="1" height="1" alt="cron" /></div>

<script src="./assets/javascript/jquery.min.js?assets_version=21"></script>
<script src="./assets/javascript/core.js?assets_version=21"></script>




<script src="./styles/prosilver/template/forum_fn.js?assets_version=21"></script>
<script src="./styles/prosilver/template/ajax.js?assets_version=21"></script>





</body>
</html>

<!DOCTYPE html>
<html dir="ltr" lang="en-gb">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<meta name="LifeViewer" content="rle code 37 hide limit">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>PotY 2020 Voting Thread - Page 3 - ConwayLife.com</title>


	<link rel="canonical" href="https://conwaylife.com/forums/viewtopic.php?t=5066&amp;start=50">

<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->

<style>
body{
	background-color:#FFF;
	background-image:url('https://www.conwaylife.com/images/back.png');
	background-position:center top;
}

#cl_navbar{font-size:150%;color:#222;position:relative;background-image:url('https://www.conwaylife.com/images/back_nav.png');background-position:center top;background-repeat:repeat-x;height:37px;margin-bottom:16px;margin-top:16px;line-height:37px;text-align:center;width:100%;border-right:1px solid #161646;border-left:1px solid #161646;}
#cl_navbar a{text-decoration:none;}
#cl_navbar a:hover{text-decoration:underline;}
</style>

<script type="text/javascript" src="./styles/prosilver/template/lv-plugin.js"></script>
<link href="./assets/css/font-awesome.min.css?assets_version=21" rel="stylesheet">
<link href="./styles/prosilver/theme/stylesheet.css?assets_version=21" rel="stylesheet">
<link href="./styles/prosilver/theme/en/stylesheet.css?assets_version=21" rel="stylesheet">




<!--[if lte IE 9]>
	<link href="./styles/prosilver/theme/tweaks.css?assets_version=21" rel="stylesheet">
<![endif]-->


<link href="./ext/phpbb/ads/styles/all/theme/phpbbads.css?assets_version=21" rel="stylesheet" media="screen" />


<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7884550756583428",
          enable_page_level_ads: true
     });
</script>

</head>
<body id="phpbb" class="nojs notouch section-viewtopic ltr ">


<div id="wrap" class="wrap">
	<a id="top" class="top-anchor" accesskey="t"></a>
	<div id="page-header">
		<div class="headerbar" role="banner">
					<div class="inner">

			<div id="site-description" class="site-description">
				<a id="logo" class="logo" href="./index.php" title="Board index"><img src="../images/forumlogo.png" width="45" height="45" alt="ConwayLife.com - A community for Conway's Game of Life and related cellular automata" /></a>
				<h1>ConwayLife.com</h1>
				<p>Forums for Conway's Game of Life</p>
				<p class="skiplink"><a href="#start_here">Skip to content</a></p>
			</div>

									<div id="search-box" class="search-box search-header" role="search">
				<form action="./search.php" method="get" id="search">
				<fieldset>
					<input name="keywords" id="keywords" type="search" maxlength="128" title="Search for keywords" class="inputbox search tiny" size="20" value="" placeholder="Search…" />
					<button class="button button-search" type="submit" title="Search">
						<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
					</button>
					<a href="./search.php" class="button button-search-end" title="Advanced search">
						<i class="icon fa-cog fa-fw" aria-hidden="true"></i><span class="sr-only">Advanced search</span>
					</a>
					
				</fieldset>
				</form>
			</div>
						
			</div>



					</div>
								<div id="cl_navbar">
									<a href="../">Home</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<a href="/wiki/">LifeWiki</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<b><a href="http://www.conwaylife.com/forums/">Forums</a></b>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<!-- <a href="/applet/">Web Applet</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp; --><a href="http://golly.sourceforge.net/">Download Golly</a>
				</div>
				<div class="navbar" role="navigation">
	<div class="inner">

	<ul id="nav-main" class="nav-main linklist" role="menubar">

		<li id="quick-links" class="quick-links dropdown-container responsive-menu" data-skip-responsive="true">
			<a href="#" class="dropdown-trigger">
				<i class="icon fa-bars fa-fw" aria-hidden="true"></i><span>Quick links</span>
			</a>
			<div class="dropdown">
				<div class="pointer"><div class="pointer-inner"></div></div>
				<ul class="dropdown-contents" role="menu">
					
											<li class="separator"></li>
																									<li>
								<a href="./search.php?search_id=unanswered" role="menuitem">
									<i class="icon fa-file-o fa-fw icon-gray" aria-hidden="true"></i><span>Unanswered topics</span>
								</a>
							</li>
							<li>
								<a href="./search.php?search_id=active_topics" role="menuitem">
									<i class="icon fa-file-o fa-fw icon-blue" aria-hidden="true"></i><span>Active topics</span>
								</a>
							</li>
							<li class="separator"></li>
							<li>
								<a href="./search.php" role="menuitem">
									<i class="icon fa-search fa-fw" aria-hidden="true"></i><span>Search</span>
								</a>
							</li>
					
										<li class="separator"></li>

									</ul>
			</div>
		</li>

				<li data-skip-responsive="true">
			<a href="/forums/app.php/help/faq" rel="help" title="Frequently Asked Questions" role="menuitem">
				<i class="icon fa-question-circle fa-fw" aria-hidden="true"></i><span>FAQ</span>
			</a>
		</li>
						
			<li class="rightside"  data-skip-responsive="true">
			<a href="./ucp.php?mode=login" title="Login" accesskey="x" role="menuitem">
				<i class="icon fa-power-off fa-fw" aria-hidden="true"></i><span>Login</span>
			</a>
		</li>
					<li class="rightside" data-skip-responsive="true">
				<a href="./ucp.php?mode=register" role="menuitem">
					<i class="icon fa-pencil-square-o  fa-fw" aria-hidden="true"></i><span>Register</span>
				</a>
			</li>
						</ul>

	<ul id="nav-breadcrumbs" class="nav-breadcrumbs linklist navlinks" role="menubar">
								<li class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
										<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope><a href="./index.php" itemtype="https://schema.org/Thing" itemprop="item" accesskey="h" data-navbar-reference="index"><i class="icon fa-home fa-fw"></i><span itemprop="name">Board index</span></a><meta itemprop="position" content="1" /></span>

											<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope data-forum-id="5"><a href="./viewforum.php?f=5" itemtype="https://schema.org/Thing" itemprop="item"><span itemprop="name">Game of Life</span></a><meta itemprop="position" content="2" /></span>
															<span class="crumb"  itemtype="http://schema.org/ListItem" itemprop="itemListElement" itemscope data-forum-id="2"><a href="./viewforum.php?f=2" itemtype="https://schema.org/Thing" itemprop="item"><span itemprop="name">Patterns</span></a><meta itemprop="position" content="3" /></span>
												</li>
		
					<li class="rightside responsive-search">
				<a href="./search.php" title="View the advanced search options" role="menuitem">
					<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
				</a>
			</li>
			</ul>

	</div>
</div>
	</div>



	
	<a id="start_here" class="anchor"></a>
	<div id="page-body" class="page-body" role="main">
		
		
<h2 class="topic-title"><a href="./viewtopic.php?f=2&amp;t=5066&amp;start=50">PotY 2020 Voting Thread</a></h2>
<!-- NOTE: remove the style="display: none" when you want to have the forum description on the topic body -->
<div style="display: none !important;">For discussion of specific patterns or specific families of patterns, both newly-discovered and well-known.<br /></div>


<div class="action-bar bar-top">
	
			<a href="./posting.php?mode=reply&amp;f=2&amp;t=5066" class="button" title="Post a reply">
							<span>Post Reply</span> <i class="icon fa-reply fa-fw" aria-hidden="true"></i>
					</a>
	
			<div class="dropdown-container dropdown-button-control topic-tools">
		<span title="Topic tools" class="button button-secondary dropdown-trigger dropdown-select">
			<i class="icon fa-wrench fa-fw" aria-hidden="true"></i>
			<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
		</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																												<li>
					<a href="./viewtopic.php?f=2&amp;t=5066&amp;start=50&amp;hilit=POTY+script&amp;view=print" title="Print view" accesskey="p">
						<i class="icon fa-print fa-fw" aria-hidden="true"></i><span>Print view</span>
					</a>
				</li>
											</ul>
		</div>
	</div>
	
			<div class="search-box" role="search">
			<form method="get" id="topic-search" action="./search.php">
			<fieldset>
				<input class="inputbox search tiny"  type="search" name="keywords" id="search_keywords" size="20" placeholder="Search this topic…" />
				<button class="button button-search" type="submit" title="Search">
					<i class="icon fa-search fa-fw" aria-hidden="true"></i><span class="sr-only">Search</span>
				</button>
				<a href="./search.php" class="button button-search-end" title="Advanced search">
					<i class="icon fa-cog fa-fw" aria-hidden="true"></i><span class="sr-only">Advanced search</span>
				</a>
				<input type="hidden" name="t" value="5066" />
<input type="hidden" name="sf" value="msgonly" />

			</fieldset>
			</form>
		</div>
	
			<div class="pagination">
			58 posts
							<ul>
			<li class="arrow previous"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" rel="prev" role="button"><i class="icon fa-chevron-left fa-fw" aria-hidden="true"></i><span class="sr-only">Previous</span></a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script" role="button">1</a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" role="button">2</a></li>
			<li class="active"><span>3</span></li>
	</ul>
					</div>
		</div>




			<div id="p125817" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile125817">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1838" class="username">Hunting</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1838&amp;sr=posts">4341</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> September 11th, 2017, 2:54 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content125817">

						<h3 class="first"><a href="#p125817">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125817" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125817#p125817" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1838" class="username">Hunting</a></strong> &raquo; </span>March 16th, 2021, 7:20 am
			</p>
			
			
			
			<div class="content">#01 **<br>
#02 ***<br>
#03 **<br>
#04 **<br>
#05 ***<br>
#06 **<br>
#07 **<br>
#08 *<br>
#09 **<br>
#11 *<br>
#12 **<br>
#13 **<br>
#15 **<br>
#16 ***<br>
#17 *<br>
#18 *<br>
#19 **<br>
#21 **<br>
#22 *<br>
#23 ***<br>
#24 **<br>
#25 **<br>
#26 **<br>
#27 **<br>
#28 **<br>
#29 ***<br>
#30 **<br>
#31 *<br>
#32 **<br>
#33 **<br>
#34 ***<br>
#36 *<br>
#37 *<br>
#38 **<br>
#39 ***</div>

			
			
									
									<div id="sig125817" class="signature">she/her<br>

<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 4, y = 5, rule = B2n3/S23-q
2bo$2bo$bobo2$3o!</code></pre></div>

I have the most experience of using rlifesrc here (other than AlephAlpha). Search requests welcome.<br>
<br>
<span style="font-size:150%;line-height:116%"><a href="https://conwaylife.com/forums/viewtopic.php?f=7&amp;t=5059" class="postlink">A LeapLife Status Report (NOW WITH LIFEVIEWER ANIMATION!)</a></span><br>
<br>
<a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4267" class="postlink">LeapLife</a> - <a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4367" class="postlink">DirtyLife</a> - <a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4418" class="postlink">LispLife</a><br>
<br>
<strong class="text-strong">Welcome back.</strong></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
				
			<div id="p125819" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile125819">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2628" class="avatar"><img class="avatar" src="./download/file.php?avatar=2628_1615616324.gif" width="78" height="78" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2628" class="username">May13</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2628&amp;sr=posts">3</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> March 11th, 2021, 8:33 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content125819">

						<h3 ><a href="#p125819">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125819" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125819#p125819" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2628" class="username">May13</a></strong> &raquo; </span>March 16th, 2021, 7:38 am
			</p>
			
			
			
			<div class="content">#01 ***<br>
#02 **<br>
#03 *<br>
#04 ***<br>
#05 ***<br>
#06 **<br>
#07 **<br>
#08 ***<br>
#09 **<br>
#10 ***<br>
#11 *<br>
#12 ***<br>
#13 ***<br>
#14 **<br>
#15 ***<br>
#16 **<br>
#17 ***<br>
#18 ***<br>
#19 ***<br>
#20 ***<br>
#21 ***<br>
#22 ***<br>
#23 ***<br>
#24 ***<br>
#25 ***<br>
#26 **<br>
#27 **<br>
#28 ***<br>
#29 ***<br>
#30 ***<br>
#31 *<br>
#32 ***<br>
#33 ***<br>
#34 ***<br>
#35 ***<br>
#36 ***<br>
#37 ***<br>
#38 ***<br>
#39 ***</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p125820" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile125820">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=1637" class="avatar"><img class="avatar" src="./download/file.php?avatar=1637_1501976378.gif" width="112" height="88" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1637" class="username">Goldtiger997</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1637&amp;sr=posts">641</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> June 21st, 2016, 8:00 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content125820">

						<h3 ><a href="#p125820">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125820" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125820#p125820" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1637" class="username">Goldtiger997</a></strong> &raquo; </span>March 16th, 2021, 7:54 am
			</p>
			
			
			
			<div class="content">#01 *<br>
#02 *<br>
#07 *<br>
#08 *<br>
#11 *<br>
#12 **<br>
#13 **<br>
#14 **<br>
#15 *<br>
#16 *<br>
#18 **<br>
#19 ***<br>
#21 *<br>
#22 **<br>
#23 *<br>
#24 **<br>
#26 *<br>
#27 *<br>
#28 **<br>
#29 *<br>
#30 *<br>
#31 *<br>
#32 **<br>
#33 **<br>
#34 ***<br>
#35 **<br>
#37 ***<br>
#38 **<br>
#39 ***</div>

			
			
									
									
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p125830" class="post has-profile bg1">
		<div class="inner">

		<dl class="postprofile" id="profile125830">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=1838" class="username">Hunting</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=1838&amp;sr=posts">4341</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> September 11th, 2017, 2:54 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content125830">

						<h3 ><a href="#p125830">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125830" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125830#p125830" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=1838" class="username">Hunting</a></strong> &raquo; </span>March 16th, 2021, 9:44 am
			</p>
			
			
			
			<div class="content">I see, Goldtiger is being <em class="text-italics">very</em> picky here. (No offense or any negative meaning here!)</div>

			
			
									
									<div id="sig125830" class="signature">she/her<br>

<div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 4, y = 5, rule = B2n3/S23-q
2bo$2bo$bobo2$3o!</code></pre></div>

I have the most experience of using rlifesrc here (other than AlephAlpha). Search requests welcome.<br>
<br>
<span style="font-size:150%;line-height:116%"><a href="https://conwaylife.com/forums/viewtopic.php?f=7&amp;t=5059" class="postlink">A LeapLife Status Report (NOW WITH LIFEVIEWER ANIMATION!)</a></span><br>
<br>
<a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4267" class="postlink">LeapLife</a> - <a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4367" class="postlink">DirtyLife</a> - <a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4418" class="postlink">LispLife</a><br>
<br>
<strong class="text-strong">Welcome back.</strong></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p125875" class="post has-profile bg2 online">
		<div class="inner">

		<dl class="postprofile" id="profile125875">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2445" class="username">Schiaparelliorbust</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2445&amp;sr=posts">3424</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> July 22nd, 2020, 9:50 am</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> Acidalia Planitia</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content125875">

						<h3 ><a href="#p125875">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125875" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125875#p125875" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2445" class="username">Schiaparelliorbust</a></strong> &raquo; </span>March 16th, 2021, 3:32 pm
			</p>
			
			
			
			<div class="content">#1 ***<br>
#2 **<br>
#3 *<br>
#4 ***<br>
#5 ***<br>
#6 **<br>
#7 **<br>
#8 ***<br>
#9 ***<br>
#10 **<br>
#11 *<br>
#12 **<br>
#13 *<br>
#14 ***<br>
#15 **<br>
#16 *<br>
#17 **<br>
#18 ***<br>
#19 **<br>
#20 *<br>
#21 ***<br>
#22 ***<br>
#23 **<br>
#24 ***<br>
#25 **<br>
#26 *<br>
#27 **<br>
#28 ***<br>
#29 **<br>
#30 ***<br>
#31 **<br>
#32 *<br>
#33 ***<br>
#34 ***<br>
#35 *<br>
#36 **<br>
#37 ***<br>
#38 ***<br>
#39 ***</div>

			
			
									
									<div id="sig125875" class="signature"><a href="https://conwaylife.com/forums/viewtopic.php?f=12&amp;t=4862" class="postlink">Hunting's language</a> (though he doesn't want me to call it that)<br>
<a href="https://www.conwaylife.com/forums/viewtopic.php?f=12&amp;t=4831" class="postlink">Board And Card Games</a><br>
<a href="https://www.conwaylife.com/forums/viewtopic.php?f=11&amp;t=4754" class="postlink">Colorised CA</a><br>
<a href="https://www.conwaylife.com/forums/viewtopic.php?f=12&amp;t=4976" class="postlink">Alien Biosphere</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p125931" class="post has-profile bg1 online">
		<div class="inner">

		<dl class="postprofile" id="profile125931">
			<dt class="no-profile-rank no-avatar">
				<div class="avatar-container">
																			</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2486&amp;sr=posts">4094</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> August 31st, 2020, 5:58 pm</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content125931">

						<h3 ><a href="#p125931">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=125931" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=125931#p125931" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2486" class="username">MathAndCode</a></strong> &raquo; </span>Yesterday, 9:13 am
			</p>
			
			
			
			<div class="content">This is a reminder that <span class="posthilit">PotY</span> 2020 voting ends in less than a day.</div>

			
			
									
									<div id="sig125931" class="signature">I have historically worked on conduits, but recently, I've been working on glider syntheses and investigating SnakeLife.</div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p126071" class="post has-profile bg2">
		<div class="inner">

		<dl class="postprofile" id="profile126071">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="avatar"><img class="avatar" src="./download/file.php?avatar=2614_1614092063.gif" width="100" height="125" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2614&amp;sr=posts">238</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 18th, 2021, 11:18 am</dd>		
		
						
						
		</dl>

		<div class="postbody">
						<div id="post_content126071">

						<h3 ><a href="#p126071">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=126071" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=126071#p126071" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2614" class="username">wwei47</a></strong> &raquo; </span>31 minutes ago
			</p>
			
			
			
			<div class="content">Are we done, or does 3/17 still exist in some parts around the world?</div>

			
			
									
									<div id="sig126071" class="signature"><div class="codebox"><p>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></p><pre><code>x = 19, y = 17, rule = Symbiosis
11.A$9.2A.A$3.A$2.3A3.A6.A$.A2.A3.2A5.A$A7.A.3A2.2A$A.3A4.A.4A.3A$.2A
2.B3.A4.A$B.B8.2A$13.A$12.2A$7.B3.2A4.2B$8.2A.A5.A$10.A3.A$9.2AB.B$8.
A2.2B4.B$8.B9.A!
</code></pre></div></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
						<div id="p126073" class="post has-profile bg1 online">
		<div class="inner">

		<dl class="postprofile" id="profile126073">
			<dt class="no-profile-rank has-avatar">
				<div class="avatar-container">
																<a href="./memberlist.php?mode=viewprofile&amp;u=2322" class="avatar"><img class="avatar" src="./download/file.php?avatar=2322_1611054705.gif" width="77" height="77" alt="User avatar" /></a>														</div>
								<a href="./memberlist.php?mode=viewprofile&amp;u=2322" class="username">yujh</a>							</dt>

									
		<dd class="profile-posts"><strong>Posts:</strong> <a href="./search.php?author_id=2322&amp;sr=posts">1927</a></dd>		<dd class="profile-joined"><strong>Joined:</strong> February 27th, 2020, 11:23 pm</dd>		
		
											<dd class="profile-custom-field profile-phpbb_location"><strong>Location:</strong> CHINA</dd>
							
						
		</dl>

		<div class="postbody">
						<div id="post_content126073">

						<h3 ><a href="#p126073">Re: <span class="posthilit">PotY</span> 2020 Voting Thread</a></h3>

													<ul class="post-buttons">
																																									<li>
							<a href="./posting.php?mode=quote&amp;f=2&amp;p=126073" title="Reply with quote" class="button button-icon-only">
								<i class="icon fa-quote-left fa-fw" aria-hidden="true"></i><span class="sr-only">Quote</span>
							</a>
						</li>
														</ul>
							
						<p class="author">
									<a class="unread" href="./viewtopic.php?p=126073#p126073" title="Post">
						<i class="icon fa-file fa-fw icon-lightgray icon-md" aria-hidden="true"></i><span class="sr-only">Post</span>
					</a>
								<span class="responsive-hide">by <strong><a href="./memberlist.php?mode=viewprofile&amp;u=2322" class="username">yujh</a></strong> &raquo; </span>10 minutes ago
			</p>
			
			
			
			<div class="content"><blockquote><div><cite><a href="./memberlist.php?mode=viewprofile&amp;u=2614">wwei47</a> wrote: <a href="./viewtopic.php?p=126071#p126071" data-post-id="126071" onclick="if(document.getElementById(hash.substr(1)))href=hash">↑</a><div class="responsive-hide">31 minutes ago</div></cite>
Are we done, or does 3/17 still exist in some parts around the world?
</div></blockquote>

I think we're not.<br>

<blockquote class="uncited"><div>    Saka wrote: ↑<br>
    March 16th, 2020, 12:25 pm<br>
    Just in time?<br>
<br>
You were way ahead of the deadline -- it's still March 15th in American Samoa (but not in Samoa) for another hour and change.</div></blockquote></div>

			
			
									
									<div id="sig126073" class="signature"><a href="https://www.conwaylife.com/forums/viewtopic.php?f=11&amp;t=4330" class="postlink">B34kz5e7c8/S23-a4ityz5k!!!</a><br>
<br>
<a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4890" class="postlink">b2n3-q5y6cn7s23-k4c8</a><br>
<br>
<a href="https://conwaylife.com/forums/viewtopic.php?f=11&amp;t=4642" class="postlink">B3-kq6cn8/S2-i3-a4ciyz8</a><br>
<br>
<a href="https://conwaylife.com/wiki/User:Yujh" class="postlink">wiki</a></div>
						</div>

		</div>

				<div class="back2top">
						<a href="#top" class="top" title="Top">
				<i class="icon fa-chevron-circle-up fa-fw icon-gray" aria-hidden="true"></i>
				<span class="sr-only">Top</span>
			</a>
					</div>
		
		</div>
	</div>

	<hr class="divider" />
	
			

	<div class="action-bar bar-bottom">
	
			<a href="./posting.php?mode=reply&amp;f=2&amp;t=5066" class="button" title="Post a reply">
							<span>Post Reply</span> <i class="icon fa-reply fa-fw" aria-hidden="true"></i>
					</a>
		
		<div class="dropdown-container dropdown-button-control topic-tools">
		<span title="Topic tools" class="button button-secondary dropdown-trigger dropdown-select">
			<i class="icon fa-wrench fa-fw" aria-hidden="true"></i>
			<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
		</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																												<li>
					<a href="./viewtopic.php?f=2&amp;t=5066&amp;start=50&amp;hilit=POTY+script&amp;view=print" title="Print view" accesskey="p">
						<i class="icon fa-print fa-fw" aria-hidden="true"></i><span>Print view</span>
					</a>
				</li>
											</ul>
		</div>
	</div>

			<form method="post" action="./viewtopic.php?f=2&amp;t=5066&amp;start=50">
		<div class="dropdown-container dropdown-container-left dropdown-button-control sort-tools">
	<span title="Display and sorting options" class="button button-secondary dropdown-trigger dropdown-select">
		<i class="icon fa-sort-amount-asc fa-fw" aria-hidden="true"></i>
		<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
	</span>
	<div class="dropdown hidden">
		<div class="pointer"><div class="pointer-inner"></div></div>
		<div class="dropdown-contents">
			<fieldset class="display-options">
							<label>Display: <select name="st" id="st"><option value="0" selected="selected">All posts</option><option value="1">1 day</option><option value="7">7 days</option><option value="14">2 weeks</option><option value="30">1 month</option><option value="90">3 months</option><option value="180">6 months</option><option value="365">1 year</option></select></label>
								<label>Sort by: <select name="sk" id="sk"><option value="a">Author</option><option value="t" selected="selected">Post time</option><option value="s">Subject</option></select></label>
				<label>Direction: <select name="sd" id="sd"><option value="a" selected="selected">Ascending</option><option value="d">Descending</option></select></label>
								<hr class="dashed" />
				<input type="submit" class="button2" name="sort" value="Go" />
						</fieldset>
		</div>
	</div>
</div>
		</form>
	
	
	
			<div class="pagination">
			58 posts
							<ul>
			<li class="arrow previous"><a class="button button-icon-only" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" rel="prev" role="button"><i class="icon fa-chevron-left fa-fw" aria-hidden="true"></i><span class="sr-only">Previous</span></a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script" role="button">1</a></li>
				<li><a class="button" href="./viewtopic.php?f=2&amp;t=5066&amp;hilit=POTY+script&amp;start=25" role="button">2</a></li>
			<li class="active"><span>3</span></li>
	</ul>
					</div>
	</div>


<div class="action-bar actions-jump">
		<p class="jumpbox-return">
		<a href="./viewforum.php?f=2" class="left-box arrow-left" accesskey="r">
			<i class="icon fa-angle-left fa-fw icon-black" aria-hidden="true"></i><span>Return to “Patterns”</span>
		</a>
	</p>
	
		<div class="jumpbox dropdown-container dropdown-container-right dropdown-up dropdown-left dropdown-button-control" id="jumpbox">
			<span title="Jump to" class="button button-secondary dropdown-trigger dropdown-select">
				<span>Jump to</span>
				<span class="caret"><i class="icon fa-sort-down fa-fw" aria-hidden="true"></i></span>
			</span>
		<div class="dropdown">
			<div class="pointer"><div class="pointer-inner"></div></div>
			<ul class="dropdown-contents">
																				<li><a href="./viewforum.php?f=6" class="jumpbox-cat-link"> <span> ConwayLife.com</span></a></li>
																<li><a href="./viewforum.php?f=3" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Website Discussion</span></a></li>
																<li><a href="./viewforum.php?f=4" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Bugs &amp; Errors</span></a></li>
																<li><a href="./viewforum.php?f=5" class="jumpbox-cat-link"> <span> Game of Life</span></a></li>
																<li><a href="./viewforum.php?f=7" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; General Discussion</span></a></li>
																<li><a href="./viewforum.php?f=11" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Other Cellular Automata</span></a></li>
																<li><a href="./viewforum.php?f=9" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Scripts</span></a></li>
																<li><a href="./viewforum.php?f=2" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; Patterns</span></a></li>
																<li><a href="./viewforum.php?f=14" class="jumpbox-cat-link"> <span> The Sandbox</span></a></li>
																<li><a href="./viewforum.php?f=12" class="jumpbox-sub-link"><span class="spacer"></span> <span>&#8627; &nbsp; The Sandbox</span></a></li>
											</ul>
		</div>
	</div>

	</div>


			</div>


<div id="page-footer" class="page-footer" role="contentinfo">
	<div class="navbar" role="navigation">
	<div class="inner">

	<ul id="nav-footer" class="nav-footer linklist" role="menubar">
		<li class="breadcrumbs">
									<span class="crumb"><a href="./index.php" data-navbar-reference="index"><i class="icon fa-home fa-fw" aria-hidden="true"></i><span>Board index</span></a></span>					</li>
		
				<li class="rightside">All times are <span title="UTC-4">UTC-04:00</span></li>
							<li class="rightside">
				<a href="./ucp.php?mode=delete_cookies" data-ajax="true" data-refresh="true" role="menuitem">
					<i class="icon fa-trash fa-fw" aria-hidden="true"></i><span>Delete cookies</span>
				</a>
			</li>
																<li class="rightside" data-last-responsive="true">
				<a href="./memberlist.php?mode=contactadmin" role="menuitem">
					<i class="icon fa-envelope fa-fw" aria-hidden="true"></i><span>Contact us</span>
				</a>
			</li>
			</ul>

	</div>
</div>

	<div class="copyright">
				<p class="footer-row">
			<span class="footer-copyright">Powered by <a href="https://www.phpbb.com/">phpBB</a>&reg; Forum Software &copy; phpBB Limited</span>
		</p>
						<p class="footer-row">
			<a class="footer-link" href="./ucp.php?mode=privacy" title="Privacy" role="menuitem">
				<span class="footer-link-text">Privacy</span>
			</a>
			|
			<a class="footer-link" href="./ucp.php?mode=terms" title="Terms" role="menuitem">
				<span class="footer-link-text">Terms</span>
			</a>
			|
			<a class="footer-link" href="http://www.njohnston.ca" role="menuitem">
				<span class="footer-link-text">Created by Nathaniel Johnston</span>
			</a>
			|
			<a class="footer-link" href="https://platprices.com" role="menuitem">
				<span class="footer-link-text">Hosted by PlatPrices</span>
			</a>
		</p>
					</div>

	<div id="darkenwrapper" class="darkenwrapper" data-ajax-error-title="AJAX error" data-ajax-error-text="Something went wrong when processing your request." data-ajax-error-text-abort="User aborted request." data-ajax-error-text-timeout="Your request timed out; please try again." data-ajax-error-text-parsererror="Something went wrong with the request and the server returned an invalid reply.">
		<div id="darken" class="darken">&nbsp;</div>
	</div>

	<div id="phpbb_alert" class="phpbb_alert" data-l-err="Error" data-l-timeout-processing-req="Request timed out.">
		<a href="#" class="alert_close">
			<i class="icon fa-times-circle fa-fw" aria-hidden="true"></i>
		</a>
		<h3 class="alert_title">&nbsp;</h3><p class="alert_text"></p>
	</div>
	<div id="phpbb_confirm" class="phpbb_alert">
		<a href="#" class="alert_close">
			<i class="icon fa-times-circle fa-fw" aria-hidden="true"></i>
		</a>
		<div class="alert_text"></div>
	</div>
</div>

</div>

<div>
	<a id="bottom" class="anchor" accesskey="z"></a>
	<img src="./cron.php?cron_type=cron.task.core.prune_notifications" width="1" height="1" alt="cron" /></div>

<script src="./assets/javascript/jquery.min.js?assets_version=21"></script>
<script src="./assets/javascript/core.js?assets_version=21"></script>




<script src="./styles/prosilver/template/forum_fn.js?assets_version=21"></script>
<script src="./styles/prosilver/template/ajax.js?assets_version=21"></script>





</body>
</html>

"""
# remove all quoted text before parsing
while votes.find("<blockquote")>0:
  bqstart = votes.find("<blockquote")
  bqend = votes.find("</blockquote>",bqstart)
  if bqend == -1:
    print("found unmatched blockquote\n"+votes[bqstart:bqstart+1000])
    break
  votes = votes[:bqstart]+votes[bqend+13:]

# look at every HTML substring that might be a line in a message
# increase the 'index' pointer after each find until no more ">.*<" are found
votelist, maxnum, index, d, totals = [], 0, 0, {}, {}
while votes.find(">",index)>-1:
  print(str(index))
  linestart = votes.find(">", index)
  lineend = votes.find("<", linestart)
  if lineend==-1:
    index = len(votes)
  else:
    line = votes[linestart+1:lineend]+"[end]"
    line = line.replace("\n","")
    print(line)
    index = lineend

    # test whether the latest line is a canonical vote line
    if line[0]=="#":
      numind = 1
      while line[numind].isdigit(): numind+=1
      numstr = line[1:numind]
      if numstr != "":
        # g.note(line + " :: " + numstr)
        num = int(numstr)
        while line[numind]==" ":
          numind+=1
        voteind = numind
        # g.note(line[voteind:voteind+10])
        count = 0
        while line[voteind]=="*":
          count+=1
          voteind+=1
        if count > 0 and count < 4:
          if num > maxnum: maxnum = num
          votelist += [[num, count]]

# build the output table from the collected votes in votelist
for i in range(maxnum):
  d[i+1] = str(i+101)[1:] + ", "
  totals[i+1] = 0
for item in votelist:
  d[item[0]] += str(item[1])+", "
  totals[item[0]] += item[1]
patterns = []

for i in range(maxnum):
  data = d[i+1]
  if data[-2:] == ", ": data = data[:-2] # remove trailing comma
  patterns+=[str(1000+totals[i+1])[1:] + ", #" + data]
sortpat = sorted(patterns, reverse=True)

s = "Total, Pattern #, Votes\n"
for item in sortpat: s+=item +"\n"
print(s)
winner = sortpat[0]
wi = winner.find(",")
print("Copied CSV output to clipboard.  Winner is " + winner[wi+2:winner.find(",",wi+2)] + " with " + winner[:wi] + " stars.")
